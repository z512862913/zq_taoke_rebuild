from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from student.views import login, logout, get_all_pop_up_notice, mark_notice_as_read
from views import pc_home_page, webapp_home_page
import settings

if settings.DEBUG:
    urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # import debug_toolbar
    # urlpatterns += [
    #     url(r'^__debug__/', include(debug_toolbar.urls)),
    # ]
else:
    urlpatterns = []

urlpatterns += [
    # Examples:
    # url(r'^$', 'zq_taoke_rebuild.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^doc/', include('doc.urls')),
    url(r'^api/course/', include('course.urls')),
    url(r'^api/source/', include('source.urls')),
    url(r'^api/student/', include('student.urls')),
    url(r'^api/auth/login/$', login),
    url(r'^api/auth/logout/$', logout),
    url(r'^api/prompt/$', get_all_pop_up_notice),
    url(r'^api/prompt/(\d{1,15})/$', mark_notice_as_read),
    url(r'^webapp', webapp_home_page),
    url(r'^', pc_home_page),
]
