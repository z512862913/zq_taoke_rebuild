# -*- coding: utf-8 -*-
import datetime

from django.db import models

# Create your models here.


# 用户留的反馈
class Suggestion(models.Model):
    GENDER_CHOICE = (
        (u'未知', u'未知'),
        (u'男', u'男'),
        (u'女', u'女'),
    )

    like = models.BooleanField(verbose_name=u"喜欢与否", default=False)
    suggestion = models.TextField(verbose_name=u"建议", max_length=2000)
    published = models.DateTimeField(verbose_name=u"提交时间", auto_now_add=True)
    student = models.ForeignKey(to='student.Student', verbose_name=u"提交学生", null=True, blank=True)
    name = models.CharField(verbose_name=u"学生姓名", max_length=40)
    gender = models.CharField(verbose_name=u"学生性别", choices=GENDER_CHOICE, max_length=4, default=u'未知')
    number = models.CharField(verbose_name=u"学号", max_length=20)
    academic = models.CharField(verbose_name=u"学院", blank=True, null=True, max_length=40)
    contact = models.CharField(verbose_name=u"联系方式", max_length=200, null=True, blank=True)

    class Meta:
        verbose_name = u"用户反馈"
        verbose_name_plural = u"用户反馈"
        ordering = ['-published']

    def __unicode__(self):
        return u"%s的反馈" % self.name


# 后台的错误代码和前端的文案的转化表
class ErrorCodeToMessage(models.Model):
    code = models.IntegerField(verbose_name=u'错误代码', db_index=True)
    message = models.CharField(verbose_name=u'前端错误提示文案', max_length=200, default=u'抱歉，服务器君遇到了未知错误')

    class Meta:
        verbose_name = u'前端提示文案'
        verbose_name_plural = u'前端提示文案'
        ordering = ['code']

    def __unicode__(self):
        return u'错误代码%s，提示用户：%s' % (self.code, self.message)

    @staticmethod
    def cast_error_code_to_message_for_front_end(error_code):
        try:
            error_model = ErrorCodeToMessage.objects.get(code=error_code)
        except ErrorCodeToMessage.DoesNotExist:
            error_model = ErrorCodeToMessage(code=error_code)
            error_model.save()
        return error_model.message


# 各种弹窗
class PopUpNotice(models.Model):
    is_disabled = models.BooleanField(verbose_name=u'是否禁用弹出', default=False)
    is_repeated = models.BooleanField(verbose_name=u'是否为重复提醒', default=False)
    pop_for_anonymous_user = models.BooleanField(verbose_name=u'是否弹出给未登录的用户', default=False)
    title = models.CharField(verbose_name=u'弹窗标题', max_length=40, blank=True, null=True)
    understand = models.CharField(verbose_name=u'用户确认明白的文案', max_length=40, blank=True, null=True)
    content = models.TextField(verbose_name=u'弹窗具体内容', blank=True, null=True)
    pop_up_url = models.CharField(verbose_name=u'出现弹窗的url', max_length=50, blank=True, null=True)
    rank = models.IntegerField(verbose_name=u'排序标号', default=1)

    class Meta:
        verbose_name = u'前端弹窗'
        verbose_name_plural = u'前端弹窗'
        ordering = ['rank']

    def __unicode__(self):
        return u'弹窗%s' % self.title

    def get_pop_up_detail(self):
        data = {
            'id': self.id,
            'is_repeated': self.is_repeated,
            'text_title': self.title,
            'text_understand': self.understand,
            'content': self.content,
            'path': self.pop_up_url,
        }
        return data

    @staticmethod
    def get_pop_up_notice_for_anonymous_user():
        data = []
        for notice in PopUpNotice.objects.filter(pop_for_anonymous_user=True).exclude(is_disabled=True):
            notice_detail = notice.get_pop_up_detail()
            # 未登录用户一律返回不是重复弹出的，这样就不会有不在弹出的选项
            # 因为未登录用户懒得跟踪session，所以每次都会弹出所有pop_for_anonymous_user == True的通知
            notice_detail['is_repeated'] = False
            data.append(notice_detail)
        return data


class StudentNoticeRelations(models.Model):
    notice = models.ForeignKey(verbose_name=u'对应的通知', to=PopUpNotice)
    student = models.ForeignKey(verbose_name=u'对应的学生', to='student.Student')
    read_date = models.DateField(verbose_name=u'通知阅读的时间', default=None, null=True)
    do_not_remind = models.BooleanField(verbose_name=u'是否不在提醒', default=False)

    class Meta:
        verbose_name = u'弹窗与学生关系中间表'
        verbose_name_plural = u'弹窗与学生关系中间表'

    def __unicode__(self):
        return u'弹窗%s与学生%s的关系' % (self.notice, self.student)

    def if_needs_to_pop(self):
        # 检查这个弹框是否需要弹出
        if not self.notice.is_disabled:
            if self.read_date == datetime.date.today():
                # 弹窗今天已经被阅读过
                return False
            else:
                if self.read_date and not self.notice.is_repeated:
                    # 弹窗曾经被阅读过，且不是重复提示的弹窗
                    return False
                elif self.do_not_remind:
                    # 设置为不再提醒的弹窗
                    return False
                else:
                    # 剩下的弹窗就是要返回的了
                    return True
        else:
            return False

