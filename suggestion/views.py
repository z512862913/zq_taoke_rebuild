# -*- coding: utf-8 -*-
import json

from django.http import JsonResponse
from models import Suggestion, ErrorCodeToMessage
from student.views import customized_login_required


@customized_login_required
def add_feedback(request):
    """
    添加反馈
    :param request: POST
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'POST':
        if not request.body:
            request_data = {}
        else:
            request_data = json.loads(request.body)
        is_like = request_data.get('is_like')
        feedback = request_data.get('feedback')
        contact = request_data.get('contact')
        Suggestion.objects.create(like=is_like, suggestion=feedback, student=student, name=student.name,
                                  gender=student.gender, number=student.number, academic=student.academy.name,
                                  contact=contact)
        status['message'] = '反馈成功'
        return JsonResponse(result, status=201)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)

