import os

current_path = os.path.abspath('.')
for dirname in os.listdir(current_path):
    if not os.path.isfile(os.path.join(current_path, dirname)):
        if os.path.exists(os.path.join(current_path, dirname+'/migrations')):
            for filename in os.listdir(os.path.join(current_path, dirname+'/migrations')):
                if not filename.startswith('__'):
                    os.remove(os.path.join(current_path, dirname+'/migrations/'+filename))

os.remove(os.path.join(current_path, 'db.sqlite3'))
