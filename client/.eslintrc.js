module.exports = {
    root: true,
    extends: 'airbnb/base',
    // required to lint *.vue files
    plugins: [
        'html'
    ],
    // add your custom rules here
    "rules": {
        // allow paren-less arrow functions
        "arrow-parens": 0,
        // allow debugger during development
        "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
        // use indent of 4 spaces
        "indent": [
            2,
            4,
            {
                "VariableDeclarator": 1,
                "SwitchCase": 1,
            }
        ],
        // use double quotes
        "quotes": [ 2, "double", "avoid-escape" ],
        // enforce spaces inside of brackets
        "array-bracket-spacing": [ 2, "always" ],
        // enforce dangling commas only-multiline
        "comma-dangle": [ 2, "always-multiline" ],
        // use stroustrup brace style
        "brace-style": [ 2, "stroustrup", { "allowSingleLine": false } ],
        // allow anonymous functions
        "func-names": 0,
        // allow string concatenation
        "prefer-template": 0,
        // allow else return
        "no-else-return": 0,
        // allow shadowing
        "no-shadow": 0,
        "strict": [ 1, "global" ],
    },
};
