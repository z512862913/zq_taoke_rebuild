import Vue from "vue";
import VueRouter from "vue-router";

import * as config from "config";

import App from "container/webapp/App";

// install plugin
Vue.use(VueRouter);

// define router
const router = new VueRouter(config.router);

// define router map rules
router.map({
    "/course": {
        component: require("container/webapp/Course"),
        subRoutes: {
            "/find": {
                name: "CourseFind",
                component: require("container/webapp/CourseFind"),
                firstLevel: true,
            },
            "/select": {
                component: require("container/webapp/CourseSelect"),
                subRoutes: {
                    "/selection": {
                        name: "CourseSelection",
                        component: require("container/webapp/CourseSelection"),
                        firstLevel: true,
                    },
                    "/collection": {
                        name: "CourseCollection",
                        component: require("container/webapp/CourseCollection"),
                        firstLevel: true,
                    },
                },
            },
            "/comment": {
                name: "CourseComment",
                component: require("container/webapp/CourseComment"),
                firstLevel: true,
            },
        },
    },
    "/detail/:number": {
        name: "CourseDetail",
        // load detail component asynchronously
        // also for code splitting
        component(resolve) {
            require([ "container/webapp/CourseDetail" ], resolve);
        },
    },
    "/comment/:number": {
        name: "Comment",
        component: require("container/webapp/Comment"),
    },
    "/login": {
        name: "Login",
        component: require("container/webapp/Login"),
    },
});

// define router redirect rules
router.redirect({
    "/course/select": "/course/select/selection",
    "/course": "/course/find",
    "*": "/course/find",
});

router.start(App, "#app");
