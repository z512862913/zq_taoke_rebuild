import Promise from "promise";

import {
    code,
    message,
    pagination as pgConfig,
} from "config";
import * as native from "native";

import * as api from "api";
import * as utils from "./util";
import * as mutations from "./mutation-type";

export function logger({ dispatch }, promise) {
    if (promise && promise.catch) {
        return promise.catch(
            error => {
                let contents = [ message.error.unknown ];
                if (error && error.status) {
                    contents = [ error.status.message ];
                }

                dispatch(mutations.SHOW_POPUP, {
                    alert: true,
                    duration: 2000,
                    contents,
                });
                return Promise.reject(error);
            }
        );
    }
    return promise;
}

export function getLoginStatus({ dispatch, state }) {
    if (state.student.status.hasLoggedIn === null) {
        return api.getLoginStatus()
            .then(
                ({ hasLoggedIn, hasSawGuide, number }) => {
                    dispatch(
                        mutations.UPDATE_STUDENT_STATUS,
                        { hasLoggedIn, hasSawGuide }
                    );
                    dispatch(
                        mutations.RECEIVE_STUDENT_ACCOUNT,
                        { number }
                    );
                }
            );
    }
    return Promise.resolve();
}

export function logIn({ dispatch }, account) {
    return api.logIn(account)
        .then(
            () => {
                dispatch(mutations.RESET_PUBLIC_ELECTIVE_ALL);
                dispatch(mutations.RECEIVE_STUDENT_ACCOUNT, account);
                dispatch(
                    mutations.UPDATE_STUDENT_STATUS,
                    {
                        hasLoggedIn: true,
                    }
                );
            }
        );
}

export function logOut({ dispatch, state }) {
    if (state.student.status.hasLoggedIn) {
        return api.logOut()
            .then(
                () => {
                    dispatch(
                        mutations.UPDATE_STUDENT_STATUS,
                        {
                            hasLoggedIn: false,
                        }
                    );
                    window.location.reload();
                }
            );
    }
    return Promise.resolve();
}

export function getPublicElectiveItem({ dispatch, state }, number) {
    const course = state.publicElective.all[number];
    if (!course || !!course.isAbstract) {
        return api.getCoursePublicElectiveItem(number)
            .then(
                course => dispatch(
                    mutations.RECEIVE_PUBLIC_ELECTIVE_ITEM,
                    number,
                    course
                )
            );
    }
    return Promise.resolve();
}

export function getPublicElective({ dispatch, state }, { params, page }) {
    const courses = state.publicElective;
    const current = courses.current;
    // const _params = Object.assign({}, courses.params, params);
    const _params = params || courses.params;
    const _page = page || courses.pagination.page;

    function startLoading() {
        dispatch(mutations.START_PUBLIC_ELECTIVE_LOADING);
    }
    function endLoading() {
        dispatch(mutations.END_PUBLIC_ELECTIVE_LOADING);
    }

    if (!!params || !current || !current[page]) {
        const instance = window.setTimeout(startLoading, 200);

        return api.getCoursePublicElective(
                Object.assign(
                    {},
                    _params,
                    {
                        page: _page,
                        perPage: pgConfig.pc.publicElective,
                    }
                )
            )
            .then(
                ({ publicElective, pagination }) => {
                    window.clearTimeout(instance);

                    const numbers = [];
                    publicElective.forEach(course => {
                        const { number } = course;
                        numbers.push(number);
                        dispatch(mutations.RECEIVE_PUBLIC_ELECTIVE_ABSTRACT_ITEM, number, course);
                    });
                    dispatch(
                        mutations.UPDATE_PUBLIC_ELECTIVE_CURRENT,
                        {
                            params,
                            pagination,
                        },
                        numbers
                    );
                }
                // ({ publicElective, pagination }) => Promise.all(
                //         publicElective.map(
                //             number => getPublicElectiveItem(
                //                 { dispatch, state },
                //                 number
                //             )
                //         )
                //     )
                //     .then(
                //         () => dispatch(
                //             mutations.UPDATE_PUBLIC_ELECTIVE_CURRENT,
                //             {
                //                 params,
                //                 pagination,
                //             },
                //             publicElective
                //         )
                //     )
            )
            .then(endLoading)
            .catch(endLoading);
    }

    dispatch(
        mutations.UPDATE_PUBLIC_ELECTIVE_CURRENT,
        {
            params,
            pagination: { page },
        }
    );

    return Promise.resolve();
}

export function updatePublicElectivePagination({ dispatch, state }, _page) {
    let page = _page;
    if (!page) {
        page = 1;
    }
    return getPublicElective({ dispatch, state }, { page });
}

export function updatePublicElectiveParams({ dispatch, state }, params) {
    return getPublicElective({ dispatch, state }, {
        page: 1,
        params,
    });
}

export function getPublicElectiveItemStudent({ dispatch, state }, number, page) {
    const students = state.publicElective.students;
    const item = students.all[number];
    if (!item || !item.list || !item.list[page]) {
        return api.getCoursePublicElectiveItemStudent(
                number,
                { page, perPage: pgConfig.pc.publicElectiveStudent }
            )
            .then(({ student, pagination }) => {
                dispatch(
                    mutations.RECEIVE_PUBLIC_ELECTIVE_STUDENT_ITEM,
                    number,
                    pagination,
                    student
                );
            });
    }
    return Promise.resolve();
}

export function updatePublicElectiveItemStudentPagination({ dispatch, state }, number, page) {
    return getPublicElectiveItemStudent(
            { dispatch, state }, number, page
        )
        .then(
            () => dispatch(
                mutations.UPDATE_PUBLIC_ELECTIVE_STUDENT_CURRENT,
                number,
                page
            )
        );
}

export function getPublicElectiveItemComment({ dispatch, state }, number, page, isForce) {
    const comments = state.publicElective.comments;
    const item = comments.all[number];
    if (!item || !item.list || isForce || !item.list[page]) {
        return api.getCoursePublicElectiveItemComment(
                number,
                { page, perPage: pgConfig.pc.publicElectiveComment }
            )
            .then(({ comment, pagination }) => {
                dispatch(
                    mutations.RECEIVE_PUBLIC_ELECTIVE_COMMENT_ITEM,
                    number,
                    pagination,
                    comment
                );
            });
    }
    return Promise.resolve();
}

export function updatePublicElectiveItemCommentPagination({ dispatch, state }, number, page) {
    return getPublicElectiveItemComment(
            { dispatch, state }, number, page
        )
        .then(
            () => dispatch(
                mutations.UPDATE_PUBLIC_ELECTIVE_COMMENT_CURRENT,
                number,
                page
            )
        );
}

export function updatePublicElectiveItemComment({ dispatch, state }, number) {
    const comments = state.publicElective.comments;
    const item = comments.all[number];
    if (!!item) {
        const { list, pagination } = item;
        const { numPages } = pagination;
        if ((typeof numPages === "number") &&
            !!list && !!list[numPages]) {
            return getPublicElectiveItemComment(
                { dispatch, state },
                number,
                numPages,
                // force updating
                true
            );
        }
    }
    return Promise.resolve();
}

export function getPublicElectiveLeaderBoard({ dispatch, state }) {
    if (!state.publicElective.leaderBoard) {
        return api.getCoursePublicElectiveLeaderBoard()
            .then(
                // ({ leaderBoard }) => dispatch(
                //     mutations.RECEIVE_PUBLIC_ELECTIVE_LEADER_BOARD,
                //     leaderBoard
                // )
                ({ leaderBoard }) => {
                    const numbers = {};
                    Object.keys(leaderBoard).forEach(key => {
                        if (!leaderBoard[key]) {
                            return;
                        }

                        numbers[key] = [];
                        leaderBoard[key].forEach(course => {
                            const { number } = course;
                            numbers[key].push(number);
                            dispatch(
                                mutations.RECEIVE_PUBLIC_ELECTIVE_ABSTRACT_ITEM,
                                number,
                                course
                            );
                        });
                    });
                    dispatch(mutations.RECEIVE_PUBLIC_ELECTIVE_LEADER_BOARD, numbers);
                }
            );
    }
    return Promise.resolve();
}

export function getPublicElectivePopular({ dispatch, state }) {
    if (!state.publicElective.popular) {
        return api.getCoursePublicElectivePopular()
            .then(
                // ({ popular }) => Promise.all(
                //         popular.map(
                //             number => getPublicElectiveItem(
                //                 { dispatch, state },
                //                 number
                //             )
                //         )
                //     )
                //     .then(
                //         () => dispatch(
                //             mutations.RECEIVE_PUBLIC_ELECTIVE_POPULAR,
                //             popular
                //         )
                //     )
                ({ popular }) => {
                    const numbers = [];
                    popular.forEach((course) => {
                        const { number } = course;
                        numbers.push(number);
                        dispatch(
                            mutations.RECEIVE_PUBLIC_ELECTIVE_ABSTRACT_ITEM,
                            number,
                            course
                        );
                    });
                    dispatch(mutations.RECEIVE_PUBLIC_ELECTIVE_POPULAR, numbers);
                }
            );
    }
    return Promise.resolve();
}

export function getProfessionalItem({ dispatch, state }, number) {
    if (!state.professional.all[number]) {
        return api.getCourseProfessionalItem(number)
            .then(
                course => dispatch(
                    mutations.RECEIVE_PROFESSIONAL_ITEM,
                    number,
                    course
                )
            );
    }
    return Promise.resolve();
}

export function getProfessional({ dispatch, state }, { params, page }) {
    const courses = state.professional;
    const current = courses.current;
    const _params = Object.assign(courses.params, params);
    const _page = page || courses.page;
    if (!!params || !current || !current[page]) {
        return api.getCourseProfessional(
                Object.assign(
                    {},
                    _params,
                    {
                        page: _page,
                        perPage: pgConfig.pc.professional,
                    }
                )
            )
            .then(
                ({ professional, pagination }) => {
                    const numbers = [];
                    professional.forEach(course => {
                        const { number } = course;
                        numbers.push(number);
                        dispatch(mutations.RECEIVE_PROFESSIONAL_ITEM, number, course);
                    });
                    dispatch(
                        mutations.UPDATE_PROFESSIONAL_CURRENT,
                        {
                            params,
                            pagination,
                        },
                        numbers
                    );
                }
                // ({ professional, pagination }) => Promise.all(
                //         professional.map(
                //             number => getProfessionalItem({ dispatch, state }, number)
                //         )
                //     )
                //     .then(
                //         () => dispatch(
                //             mutations.UPDATE_PROFESSIONAL_CURRENT,
                //             {
                //                 params,
                //                 pagination,
                //             },
                //             professional
                //         )
                //     )
            );
    }

    dispatch(
        mutations.UPDATE_PROFESSIONAL_CURRENT,
        {
            params,
            pagination: { page },
        }
    );

    return Promise.resolve();
}

export function updateProfessionalPagination({ dispatch, state }, page) {
    return getProfessional({ dispatch, state }, { page });
}

export function updateProfessionalParams({ dispatch, state }, params) {
    return getProfessional({ dispatch, state }, {
        page: 1,
        params,
    });
}

export function getDepartment({ dispatch, state }) {
    if (!state.department.list) {
        return api.getCourseDepartment()
            .then(
                ({ department }) => dispatch(
                    mutations.RECEIVE_DEPARTMENT,
                    department
                )
            );
    }
    return Promise.resolve();
}

export function getStudentProfile({ dispatch, state }) {
    if (!state.student.profile) {
        return api.getStudentProfile()
            .then(
                profile => dispatch(
                    mutations.RECEIVE_STUDENT_PROFILE,
                    profile
                )
            );
    }
    return Promise.resolve();
}

export function updateStudentAvatar({ dispatch }) {
    return api.updateStudentAvatar()
        .then(
            ({ avatar }) => dispatch(
                mutations.UPDATE_STUDENT_PROFILE,
                { avatar }
            )
        );
}

export function saveStudentAvatar({ dispatch }, form) {
    return api.saveStudentAvatar(form)
        .then(
            ({ avatar }) => dispatch(
                mutations.UPDATE_STUDENT_PROFILE,
                { avatar }
            )
        );
}

export function getStudentCurriculum({ dispatch, state }) {
    if (!state.student.curriculum) {
        return api.getStudentCurriculum()
            .then(
                ({ curriculum }) => dispatch(
                    mutations.RECEIVE_STUDENT_CURRICULUM,
                    curriculum
                )
            );
    }
    return Promise.resolve();
}

export function getStudentSelected({ dispatch, state }) {
    if (!state.student.selected) {
        return api.getStudentPublicElectiveSelected()
            .then(
                ({ selected }) => dispatch(
                    mutations.RECEIVE_STUDENT_SELECTED,
                    selected
                )
            );
    }
    return Promise.resolve();
}

export function getStudentUncomment({ dispatch, state }) {
    if (!state.student.uncomment) {
        return api.getStudentPublicElectiveUncomment()
            .then(
                // ({ uncomment }) => Promise.all(
                //     uncomment.map(
                //         number => getPublicElectiveItem(
                //             { dispatch, state },
                //             number
                //         )
                //     )
                // )
                // .then(() => dispatch(
                //     mutations.RECEIVE_STUDENT_UNCOMMENT,
                //     uncomment
                // ))
                ({ uncomment }) => {
                    const numbers = [];
                    uncomment.forEach(course => {
                        const { number } = course;
                        numbers.push(number);
                        dispatch(mutations.RECEIVE_PUBLIC_ELECTIVE_ABSTRACT_ITEM, number, course);
                    });
                    dispatch(mutations.RECEIVE_STUDENT_UNCOMMENT, numbers);
                }
            );
    }
    return Promise.resolve();
}

export function deleteStudentUncommentItem({ dispatch, state }, { number, canComment }, form) {
    if (!!canComment) {
        return api.deleteStudentPublicElectiveUncommentItem(
                number,
                form
            )
            .then(() => {
                dispatch(
                    mutations.DELETE_STUDENT_UNCOMMENT_ITEM,
                    number
                );
                dispatch(
                    mutations.UPDATE_PUBLIC_ELECTIVE_ITEM,
                    number,
                    { canComment: false }
                );

                return updatePublicElectiveItemComment(
                    { dispatch, state },
                    number
                );
            });
    }
    return Promise.resolve();
}

export function getStudentCollected({ dispatch, state }) {
    if (!state.student.collected) {
        return api.getStudentPublicElectiveCollected()
            .then(
                // ({ collected }) => Promise.all(
                //     collected.map(
                //         number => getPublicElectiveItem(
                //             { dispatch, state },
                //             number
                //         )
                //     )
                // )
                // .then(() => dispatch(
                //     mutations.RECEIVE_STUDENT_COLLECTED,
                //     collected
                // ))
                ({ collected }) => {
                    const numbers = [];
                    collected.forEach(course => {
                        const { number } = course;
                        numbers.push(number);
                        dispatch(mutations.RECEIVE_PUBLIC_ELECTIVE_ITEM, number, course);
                    });
                    dispatch(mutations.RECEIVE_STUDENT_COLLECTED, numbers);
                }
            );
    }
    return Promise.resolve();
}

export function saveStudentCollectedItem({ dispatch, state }, { number, hasCollected }) {
    if (!hasCollected) {
        return logger(
            { dispatch },
            api.saveStudentPublicElectiveCollectedItem(number)
                .then(() => {
                    dispatch(
                        mutations.SAVE_STUDENT_COLLECTED_ITEM,
                        number
                    );
                    dispatch(
                        mutations.UPDATE_PUBLIC_ELECTIVE_ITEM,
                        number,
                        { hasCollected: true }
                    );
                })
        );
    }
    return Promise.resolve();
}

export function deleteStudentCollectedItem({ dispatch, state }, { number, hasCollected }) {
    if (!!hasCollected) {
        return logger(
            { dispatch },
            api.deleteStudentPublicElectiveCollectedItem(number)
                .then(
                    () => {
                        dispatch(
                            mutations.DELETE_STUDENT_COLLECTED_ITEM,
                            number
                        );
                        dispatch(
                            mutations.UPDATE_PUBLIC_ELECTIVE_ITEM,
                            number,
                            { hasCollected: false }
                        );
                    }
                )
        );
    }
    return Promise.resolve();
}

export function getStudentSelecting({ dispatch, state }) {
    if (!state.student.selecting) {
        return logger(
            { dispatch },
            api.getStudentPublicElectiveSelecting()
                .then(
                    // ({ selecting }) => Promise.all(
                    //         selecting.map(
                    //             number => getPublicElectiveItem(
                    //                 { dispatch, state },
                    //                 number
                    //             )
                    //         )
                    //     )
                    //     .then(() => dispatch(
                    //         mutations.RECEIVE_STUDENT_SELECTING,
                    //         selecting
                    //     ))
                    ({ selecting, failed, circulating }) => {
                        let numbers = [];
                        selecting.forEach(course => {
                            const { number } = course;
                            numbers.push(number);
                            dispatch(
                                mutations.RECEIVE_PUBLIC_ELECTIVE_ITEM,
                                number,
                                course
                            );
                        });
                        dispatch(mutations.RECEIVE_STUDENT_SELECTING, numbers);

                        numbers = [];
                        failed.forEach(course => {
                            const { number } = course;
                            numbers.push(number);
                            dispatch(
                                mutations.RECEIVE_PUBLIC_ELECTIVE_ITEM,
                                number,
                                course
                            );
                        });
                        dispatch(mutations.RECEIVE_STUDENT_SELECTING_FAILED, numbers);

                        numbers = [];
                        circulating.forEach(course => {
                            const { number } = course;
                            numbers.push(number);
                            dispatch(
                                mutations.RECEIVE_PUBLIC_ELECTIVE_ITEM,
                                number,
                                course
                            );
                        });
                        dispatch(mutations.RECEIVE_STUDENT_CIRCULATING, numbers);
                    }
                )
        );
    }
    return Promise.resolve();
}

export function updateStudentSelectingItem({ dispatch, state }, { number, hasCirculated }) {
    const isCirculate = !hasCirculated;
    return logger(
        { dispatch },
        api.updateStudentPublicElectiveSelectingItem(
                number,
                { isCirculate }
            )
            .then(
                () => {
                    dispatch(
                        mutations.UPDATE_PUBLIC_ELECTIVE_ITEM,
                        number,
                        { hasCirculated: isCirculate }
                    );
                    dispatch(
                        isCirculate ?
                        mutations.SAVE_STUDENT_CIRCULATING_ITEM :
                        mutations.DELETE_STUDENT_CIRCULATING_ITEM,
                        number
                    );
                }
            )
    );
}

export function saveStudentSelectingItem({ dispatch, state }, { number, hasCollected }) {
    return api.saveStudentPublicElectiveSelectingItem(
            number,
            { isCirculate: false }
        )
        .then(
            () => dispatch(
                mutations.SAVE_STUDENT_SELECTING_ITEM,
                number
            )
        )
        .catch(
            error => {
                function _throw() {
                    return logger({ dispatch }, Promise.reject(error));
                }

                if (!hasCollected) {
                    let message;
                    if (error && error.status) {
                        message = error.status.message;
                    }

                    return saveStudentCollectedItem(
                            { dispatch, state },
                            { number, hasCollected }
                        )
                        .catch(() => _throw())
                        .then(
                            () => {
                                const pub = code.publicElective;
                                const _code = pub.COLLECTED_AFTER_SAVE_SELECTING_FAILED;
                                return Promise.reject({
                                    status: {
                                        message,
                                        code: _code,
                                    },
                                });
                            }
                        );
                }

                return _throw();
            }
        );
}

export function deleteStudentSelectingItem({ dispatch, state }, { number }) {
    return logger(
        { dispatch },
        api.deleteStudentPublicElectiveSelectingItem(number)
            .then(
                () => dispatch(
                    mutations.DELETE_STUDENT_SELECTING_ITEM,
                    number
                )
            )
    );
}

export function getStudentProfessional({ dispatch, state }) {
    if (!state.student.professional) {
        return api.getStudentProfessional()
            .then(
                ({ professional }) => {
                    const list = {};
                    Object.keys(professional).forEach(
                        key => {
                            professional[key].forEach(course => {
                                const { number } = course;
                                dispatch(mutations.RECEIVE_PROFESSIONAL_ITEM, number, course);
                            });

                            list[key] = professional[key].map(({ number }) => number);
                        }
                    );

                    dispatch(
                        mutations.RECEIVE_STUDENT_PROFESSIONAL,
                        list
                    );
                }
                // ({ professional }) => Promise.all(
                //             key => Promise.all(
                //                 professional[key].map(
                //                     number => getProfessionalItem(
                //                         { dispatch, state },
                //                         number
                //                     )
                //                 )
                //             )
                //         )
                //     )
                //     .then(
                //         () => dispatch(
                //             mutations.RECEIVE_STUDENT_PROFESSIONAL,
                //             professional
                //         )
                //     )
            )
            .catch(x => window.console.log(x));
    }
    return Promise.resolve();
}

export function getStudentProfessionalRecommended({ dispatch, state }) {
    if (!state.student.professionalRecommended) {
        return api.getStudentProfessionalRecommended()
            .then(
                ({ recommended }) => {
                    recommended.forEach(
                        course => {
                            const { number } = course;
                            dispatch(mutations.RECEIVE_PROFESSIONAL_ITEM, number, course);
                        }
                    );

                    const numbers = recommended.map(({ number }) => number);
                    dispatch(
                        mutations.RECEIVE_STUDENT_PROFESSIONAL_RECOMMENDED,
                        numbers
                    );
                }
                // ({ recommended }) => Promise.all(
                //         recommended.map(
                //             number => getProfessionalItem(
                //                 { dispatch, state },
                //                 number
                //             )
                //         )
                //     )
                //     .then(
                //         () => dispatch(
                //             mutations.RECEIVE_STUDENT_PROFESSIONAL_RECOMMENDED,
                //             recommended
                //         )
                //     )
            );
    }
    return Promise.resolve();
}

export function saveStudentProfessionalRecommendedItem({ dispatch, state }, { number }) {
    return logger(
        { dispatch },
        api.saveStudentProfessionalRecommendedItem(number)
            .then(
                () => dispatch(
                    mutations.SAVE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM,
                    number
                )
            )
    );
}

export function deleteStudentProfessionalRecommendedItem({ dispatch, state }, { number }) {
    return api.deleteStudentProfessionalRecommendedItem(number)
        .then(
            () => dispatch(
                mutations.DELETE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM,
                number
            )
        );
}

export function saveStudentFeedback({ dispatch, state }, form) {
    return api.saveStudentFeedback(form);
}

export function getStudentSms({ dispatch, state }) {
    if (!state.student.sms) {
        return api.getStudentSms()
            .then(sms => dispatch(mutations.UPDATE_STUDENT_SMS, sms));
    }
    return Promise.resolve();
}

export function updateStudentSms({ dispatch }, sms) {
    return api.updateStudentSms(sms)
        .then(() => dispatch(mutations.UPDATE_STUDENT_SMS, sms));
}

export function deleteStudentSms({ dispatch }) {
    return api.deleteStudentSms()
        .then(() => dispatch(mutations.UPDATE_STUDENT_SMS, { smsService: false }));
}

export function getSourceCategory({ dispatch, state }, { params, page }) {
    const source = state.source;
    const _params = Object.assign({}, source.params, params || {});
    const _page = page || source.current.pagination.page;
    if (!utils.hasLoadedSourceCategory(state, _params, _page)) {
        return api.getSourceCategory(
                _params, {
                    page: _page,
                    perPage: pgConfig.pc.source,
                }
            )
            .then(({ source, pagination }) => {
                dispatch(
                    mutations.RECEIVE_SOURCE_CATEGORY,
                    source,
                    { params: _params, pagination }
                );
                return Promise.resolve({ params, pagination });
            });
    }
    return Promise.resolve({
        params,
        pagination: { page: _page },
    });
}

export function updateSourceCategoryParams({ dispatch, state }, params) {
    return getSourceCategory({ dispatch, state }, { params })
        .then(
            data => dispatch(
                mutations.UPDATE_SOURCE_CATEGORY_CURRENT,
                data
            )
        );
}

export function updateSourceCategoryPagination({ dispatch, state }, page) {
    return getSourceCategory({ dispatch, state }, { page })
        .then(
            data => dispatch(
                mutations.UPDATE_SOURCE_CATEGORY_CURRENT,
                data
            )
        );
}

export function getSource({ dispatch, state }) {
    if (!state.source.categories) {
        return api.getSource()
            .then(({ categories }) => {
                dispatch(mutations.RECEIVE_SOURCE, categories);

                if ((categories instanceof Array) && (categories.length > 0)) {
                    const { id, subcategories } = {} = categories[0];
                    if ((subcategories instanceof Array) && (subcategories.length > 0)) {
                        return updateSourceCategoryParams(
                            { dispatch, state },
                            {
                                category: id,
                                subcategory: subcategories[0].id,
                            }
                        );
                    }
                }

                return Promise.resolve();
            });
    }
    return Promise.resolve();
}

export function getSourceCategoryItem({ dispatch, state }, source) {
    return api.getSourceCategoryItem(
            Object.assign(
                {},
                state.source.params,
                source
            )
        );
}

export function getPrompt({ dispatch }) {
    return api.getPrompt()
        .then(
            ({ prompt }) => dispatch(
                mutations.RECEIVE_PROMPT,
                prompt
            )
        );
}

export function deletePromptItem({ dispatch, state }, id, form) {
    return api.deletePromptItem(id, form);
}

export function showSearch({ dispatch }) {
    dispatch(mutations.SHOW_SEARCH);
}

export function showFilter({ dispatch }) {
    dispatch(mutations.SHOW_FILTER);
}

export function hideFilter({ dispatch }) {
    dispatch(mutations.HIDE_FILTER);
}

export function showPopup({ dispatch }, opts) {
    const options = opts;
    if (!(options.contents instanceof Array)) {
        options.contents = [ options.contents ];
    }
    dispatch(mutations.SHOW_POPUP, options);
}

export function hidePopup({ dispatch }) {
    dispatch(mutations.HIDE_POPUP);
}

export function back({ dispatch, state }) {
    if (state.webapp.histories.length > 0) {
        dispatch(mutations.REVERT_HISTORY);
        return;
    }

    const route = this.$router.app.$route;
    if (Object.keys(route.query).length > 0) {
        route.router.go(route.path.split("?")[0]);
        return;
    }

    const isFirstLevelPath = route.firstLevel;
    if (isFirstLevelPath && native.isHybrid) {
        native.hybridHandler.back();
        return;
    }

    window.history.go(-1);
}
