import {
    SHOW_POPUP,
    HIDE_POPUP,
} from "../mutation-type";

// initial state
const state = {
    popup: {
        cb: null,
        duration: null,
        still: null,
        enable: false,
        title: null,
        contents: null,
    },
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [SHOW_POPUP](state, options) {
        state.popup = Object.assign({ enable: true }, options);
    },

    [HIDE_POPUP](state) {
        state.popup.enable = false;
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
