import {
    SHOW_SEARCH,
    SHOW_FILTER,
    HIDE_FILTER,
    SHOW_POPUP,
    HIDE_POPUP,
    REVERT_HISTORY,
} from "../mutation-type";

// initial state
const state = {
    search: false,
    filter: false,
    popup: {
        cb: null,
        alert: null,
        duration: null,
        enable: false,
    },
    // item's share
    // { mutation, args }
    histories: [],
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [SHOW_SEARCH](state) {
        state.search = true;
        state.histories.push(SHOW_SEARCH);
    },

    [SHOW_FILTER](state) {
        state.filter = true;
        state.histories.push(SHOW_FILTER);
    },

    [HIDE_FILTER](state) {
        state.filter = false;
        state.histories = state.histories.filter(mutation => (mutation !== SHOW_FILTER));
    },

    [SHOW_POPUP](state, options) {
        state.popup = Object.assign({ enable: true }, options);
        state.histories.push(SHOW_POPUP);
    },

    [HIDE_POPUP](state) {
        state.popup.enable = false;
        state.histories = state.histories.filter(mutation => (mutation !== SHOW_POPUP));
    },

    [REVERT_HISTORY](state) {
        if (state.histories.length > 0) {
            const lastMutation = state.histories.pop();
            switch (lastMutation) {
                case SHOW_SEARCH:
                    state.search = false;
                    break;

                case SHOW_FILTER:
                    state.filter = false;
                    break;

                case SHOW_POPUP:
                    state.popup = {
                        id: null,
                        enable: false,
                    };
                    break;

                default:
                    break;
            }
        }
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
