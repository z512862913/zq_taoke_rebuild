import {
    RECEIVE_DEPARTMENT,
} from "../mutation-type";

// initial state
const state = {
    list: null,
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [RECEIVE_DEPARTMENT](state, department) {
        state.list = department;
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
