import Vue from "vue";
import Vuex from "vuex";

import pc from "./module/pc";
import webapp from "./module/webapp";
import publicElective from "./module/public-elective";
import professional from "./module/professional";
import department from "./module/department";
import source from "./module/source";
import student from "./module/student";
import prompt from "./module/prompt";

// install plugin
Vue.use(Vuex);

export default {
    pc: new Vuex.Store({
        modules: {
            pc,
            publicElective,
            professional,
            department,
            source,
            student,
            prompt,
        },
    }),
    webapp: new Vuex.Store({
        modules: {
            webapp,
            publicElective,
            student,
        },
    }),
};
