/* eslint-disable max-len */
// auth
export const UPDATE_STUDENT_STATUS = "update student status";
export const RECEIVE_STUDENT_ACCOUNT = "receive student account";
export const RECEIVE_STUDENT_PROFILE = "receive student profile";
export const UPDATE_STUDENT_PROFILE = "update student profile";
export const RECEIVE_STUDENT_CURRICULUM = "receive student curriculum";
export const RECEIVE_STUDENT_SELECTED = "receive student selected";
export const RECEIVE_STUDENT_UNCOMMENT = "receive student uncomment";
export const RECEIVE_STUDENT_COLLECTED = "receive student collected";
export const SAVE_STUDENT_COLLECTED_ITEM = "save student collected item";
export const DELETE_STUDENT_COLLECTED_ITEM = "delete student collected item";
export const RECEIVE_STUDENT_SELECTING = "receive student selecting";
export const SAVE_STUDENT_SELECTING_ITEM = "save student selecting item";
export const DELETE_STUDENT_SELECTING_ITEM = "delete student selecting item";
export const RECEIVE_STUDENT_SELECTING_FAILED = "receive student selecting failed";
export const RECEIVE_STUDENT_CIRCULATING = "receive student circulating";
export const SAVE_STUDENT_CIRCULATING_ITEM = "save student circulating item";
export const DELETE_STUDENT_CIRCULATING_ITEM = "delete student circulating item";
export const DELETE_STUDENT_UNCOMMENT_ITEM = "delete student uncomment item";
export const RECEIVE_STUDENT_PROFESSIONAL = "receive student professional";
export const RECEIVE_STUDENT_PROFESSIONAL_RECOMMENDED = "receive student professional recommended";
export const SAVE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM = "save student professional recommended item";
export const DELETE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM = "deleteave student professional recommended item";
export const UPDATE_STUEDNT_SMS = "update student sms";

// public elective
export const START_PUBLIC_ELECTIVE_LOADING = "start public elective loading";
export const END_PUBLIC_ELECTIVE_LOADING = "end public elective loading";
export const RECEIVE_PUBLIC_ELECTIVE = "receive public elective";
export const RESET_PUBLIC_ELECTIVE_ALL = "reset public elective all";
export const UPDATE_PUBLIC_ELECTIVE_CURRENT = "update public elective current";
export const RECEIVE_PUBLIC_ELECTIVE_ITEM = "receive public elective item";
export const RECEIVE_PUBLIC_ELECTIVE_ABSTRACT_ITEM = "receive public elective abstract item";
export const UPDATE_PUBLIC_ELECTIVE_ITEM = "update public elective item";
export const RECEIVE_PUBLIC_ELECTIVE_STUDENT_ITEM = "receive public elective student item";
export const UPDATE_PUBLIC_ELECTIVE_STUDENT_PAGINTION = "update public elective student pagination";
export const UPDATE_PUBLIC_ELECTIVE_STUDENT_CURRENT = "update public elective student current";
export const RECEIVE_PUBLIC_ELECTIVE_COMMENT_ITEM = "receive public elective comment item";
export const UPDATE_PUBLIC_ELECTIVE_COMMENT_PAGINTION = "update public elective comment pagination";
export const UPDATE_PUBLIC_ELECTIVE_COMMENT_CURRENT = "update public elective comment current";
export const RECEIVE_PUBLIC_ELECTIVE_LEADER_BOARD = "receive public elective leader board";
export const RECEIVE_PUBLIC_ELECTIVE_POPULAR = "receive public elective popular";

// professional
export const UPDATE_PROFESSIONAL_CURRENT = "update professional current";
export const RECEIVE_PROFESSIONAL_ITEM = "receive professional item";

// department
export const RECEIVE_DEPARTMENT = "receive department";

// source
export const RECEIVE_SOURCE = "receive source";
export const RECEIVE_SOURCE_CATEGORY = "receive source category";
export const UPDATE_SOURCE_CATEGORY_CURRENT = "update source category current";

// prompt
export const RECEIVE_PROMPT = "receive prompt";
export const DELETE_PROMPT_ITEM = "delete prompt item";

// webapp only
export const SHOW_SEARCH = "show search";
export const SHOW_FILTER = "show filter";
export const HIDE_FILTER = "hide filter";
export const SHOW_POPUP = "show popup";
export const HIDE_POPUP = "hide popup";
export const REVERT_HISTORY = "revert history";
/* eslint-enable max-len */
