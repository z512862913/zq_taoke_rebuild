import Vue from "vue";
import VueResource from "vue-resource";

// install plugin
Vue.use(VueResource);

/* eslint-disable max-len */
export const authLogin = Vue.resource("/api/auth/login/");
export const authLogout = Vue.resource("/api/auth/logout/");

export const student = Vue.resource("/api/student/");
export const studentAvatar = Vue.resource("/api/student/avatar/");
export const studentAvatarRandom = Vue.resource("/api/student/avatar/random/");
export const studentCurriculum = Vue.resource("/api/student/curriculum/");
export const studentPublicElectiveSelected = Vue.resource("/api/student/public/elective/selected/");
export const studentPublicElectiveCollected = Vue.resource("/api/student/public/elective/collected/");
export const studentPublicElectiveCollectedItem = Vue.resource("/api/student/public/elective/collected{/number}/");
export const studentPublicElectiveUncomment = Vue.resource("/api/student/public/elective/uncomment/");
export const studentPublicElectiveUncommentItem = Vue.resource("/api/student/public/elective/uncomment{/number}/");
export const studentPublicElectiveSelecting = Vue.resource("/api/student/public/elective/selecting/");
export const studentPublicElectiveSelectingItem = Vue.resource("/api/student/public/elective/selecting{/number}/");
export const studentPublicElectiveSelectingFailed = Vue.resource("/api/student/public/elective/selecting/failed/");
export const studentProfessional = Vue.resource("/api/student/professional/");
export const studentProfessionalRecommended = Vue.resource("/api/student/professional/recommended/");
export const studentProfessionalRecommendedItem = Vue.resource("/api/student/professional/recommended{/number}/");
export const studentFeedback = Vue.resource("/api/student/feedback/");
export const studentSms = Vue.resource("/api/student/sms/");

export const coursePublicElective = Vue.resource("/api/course/public/elective/");
export const coursePublicElectiveItem = Vue.resource("/api/course/public/elective{/number}/");
export const coursePublicElectiveItemComment = Vue.resource("/api/course/public/elective{/number}/comment/");
export const coursePublicElectiveItemStudent = Vue.resource("/api/course/public/elective{/number}/student/");
export const coursePublicElectiveLeaderBoard = Vue.resource("/api/course/public/elective/leader_board/");
export const coursePublicElectivePopular = Vue.resource("/api/course/public/elective/popular/");
export const courseProfessional = Vue.resource("/api/course/professional/");
export const courseProfessionalItem = Vue.resource("/api/course/professional{/number}/");
export const courseDepartment = Vue.resource("/api/course/department/");

export const source = Vue.resource("/api/source/");
export const sourceCategory = Vue.resource("/api/source{/category}{/subcategory}/");
export const sourceCategoryItem = Vue.resource("/api/source{/category}{/subcategory}{/id}/");

export const prompt = Vue.resource("/api/prompt/");
export const promptItem = Vue.resource("/api/prompt{/id}/");
/* eslint-enable max-len */

