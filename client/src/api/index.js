import * as resource from "./resource";

import Promise from "promise";

function extractData(promise) {
    return promise.then(response => {
        if (response && response.data) {
            return Promise.resolve(response.data);
        }
        return Promise.resolve(response);
    });
}

function transformProperties(obj, isUnderToCamel = true) {
    if (typeof obj !== "object") {
        return obj;
    }
    if ((obj === null) || (obj === undefined)) {
        return null;
    }

    function underToCamel(str) {
        return str.replace(/_(.|$)/g, (str, chr) => chr.toUpperCase());
    }

    function camelToUnder(str) {
        return str.replace(/[A-Z]/g, chr => `_${chr.toLowerCase()}`);
    }

    if (obj instanceof Array) {
        return obj.map(item => transformProperties(item, isUnderToCamel));
    }

    const ret = {};
    for (const property in obj) {
        if (obj.hasOwnProperty(property)) {
            const _property = isUnderToCamel ? underToCamel(property) : camelToUnder(property);
            ret[_property] = transformProperties(obj[property], isUnderToCamel);
        }
    }

    return ret;
}

export function getLoginStatus() {
    return extractData(resource.authLogin.get())
        .then(data => Promise.resolve(transformProperties(data)));
}

export function logIn(account) {
    return resource.authLogin.save({}, account);
}

export function logOut() {
    return resource.authLogout.get();
}

export function getStudentProfile() {
    return extractData(resource.student.get());
}

export function getStudentAvatar() {
    return extractData(resource.studentAvatar.get());
}

export function updateStudentAvatar() {
    return extractData(resource.studentAvatar.update());
}

export function saveStudentAvatar(form) {
    return extractData(resource.studentAvatar.save({}, form));
}

export function getStudentAvatarRandom() {
    return extractData(resource.studentAvatarRandom.get());
}

export function getStudentCurriculum() {
    return extractData(resource.studentCurriculum.get())
        .then(data => Promise.resolve(transformProperties(data)));
}

export function getStudentPublicElectiveSelected() {
    return extractData(resource.studentPublicElectiveSelected.get())
        .then(data => transformProperties(data));
}

export function getStudentPublicElectiveCollected() {
    return extractData(resource.studentPublicElectiveCollected.get())
        .then(data => transformProperties(data));
}

export function saveStudentPublicElectiveCollectedItem(number) {
    return resource.studentPublicElectiveCollectedItem.save({ number }, {});
}

export function deleteStudentPublicElectiveCollectedItem(number) {
    return resource.studentPublicElectiveCollectedItem.delete({ number }, {});
}

export function getStudentPublicElectiveUncomment() {
    return extractData(resource.studentPublicElectiveUncomment.get())
        .then(data => transformProperties(data));
}

export function deleteStudentPublicElectiveUncommentItem(number, form) {
    return resource.studentPublicElectiveUncommentItem.delete({ number }, form);
}

export function getStudentPublicElectiveSelecting() {
    return extractData(resource.studentPublicElectiveSelecting.get())
        .then(data => transformProperties(data));
}

export function saveStudentPublicElectiveSelectingItem(number, form) {
    return resource.studentPublicElectiveSelectingItem.save(
        { number },
        transformProperties(form, false)
    );
}

export function updateStudentPublicElectiveSelectingItem(number, form) {
    return resource.studentPublicElectiveSelectingItem.update(
        { number },
        transformProperties(form, false)
    );
}

export function deleteStudentPublicElectiveSelectingItem(number) {
    return resource.studentPublicElectiveSelectingItem.delete({ number });
}

export function getStudentPublicElectiveSelectingFailed() {
    return extractData(resource.studentPublicElectiveSelectingFailed.get());
}

export function getStudentProfessional() {
    return extractData(resource.studentProfessional.get());
}

export function getStudentProfessionalRecommended() {
    return extractData(resource.studentProfessionalRecommended.get());
}

export function saveStudentProfessionalRecommendedItem(number) {
    return resource.studentProfessionalRecommendedItem.save({ number }, {});
}

export function deleteStudentProfessionalRecommendedItem(number) {
    return resource.studentProfessionalRecommendedItem.delete({ number });
}

export function saveStudentFeedback(form) {
    return resource.studentFeedback.save(
        {},
        transformProperties(form, false)
    );
}

export function getStudentSms() {
    return extractData(resource.studentSms.get())
        .then(data => Promise.resolve(transformProperties(data)));
}

export function updateStudentSms(sms) {
    return resource.studentSms.update({}, transformProperties(sms, false));
}

export function deleteStudentSms() {
    return resource.studentSms.delete();
}

export function getCoursePublicElective(form) {
    // using POST method instead of GET
    // to avoid too many arguments
    return extractData(resource.coursePublicElective.save(
            {},
            transformProperties(form, false))
        )
        .then(data => Promise.resolve(transformProperties(data)));
}

export function getCoursePublicElectiveItem(number) {
    return extractData(resource.coursePublicElectiveItem.get({ number }))
        .then(data => Promise.resolve(transformProperties(data)));
}

export function getCoursePublicElectiveItemComment(number, form) {
    return extractData(
        resource.coursePublicElectiveItemComment.get(
                { number },
                transformProperties(form, false)
            )
            .then(data => Promise.resolve(transformProperties(data)))
    );
}

export function getCoursePublicElectiveItemStudent(number, form) {
    return extractData(
        resource.coursePublicElectiveItemStudent.get(
                { number },
                transformProperties(form, false)
            )
            .then(data => Promise.resolve(transformProperties(data)))
    );
}

export function getCoursePublicElectiveLeaderBoard() {
    return extractData(resource.coursePublicElectiveLeaderBoard.get())
        .then(data => Promise.resolve(transformProperties(data)));
}

export function getCoursePublicElectivePopular() {
    return extractData(resource.coursePublicElectivePopular.get());
}

export function getCourseProfessional(form) {
    return extractData(
            resource.courseProfessional.get(
                {},
                transformProperties(form, false)
            )
        )
        .then(
            data => Promise.resolve(
                transformProperties(data)
            )
        );
}

export function getCourseProfessionalItem(number) {
    return extractData(
            resource.courseProfessionalItem.get({ number })
        )
        .then(
            data => Promise.resolve(
                transformProperties(data)
            )
        );
}

export function getCourseDepartment() {
    return extractData(resource.courseDepartment.get());
}

export function getSource() {
    return extractData(resource.source.get())
        .then(data => Promise.resolve(transformProperties(data)));
}

export function getSourceCategory(params, form) {
    return extractData(resource.sourceCategory.get(
            params,
            transformProperties(form, false)
        ))
        .then(
            data => Promise.resolve(
                transformProperties(data)
            )
        );
}

export function getSourceCategoryItem(params) {
    return extractData(
        resource.sourceCategoryItem.get(
            transformProperties(params, false)
        )
    );
}

export function getPrompt() {
    return extractData(resource.prompt.get())
        .then(
            data => Promise.resolve(
                transformProperties(data)
            )
        );
}

export function deletePromptItem(id, form) {
    return resource.promptItem.delete({ id }, form);
}
