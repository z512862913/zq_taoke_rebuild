import Android from "./Android";
import iOS from "./iOS";

const hybridInstance = Android.instance || iOS.instance;

export const isHybrid = !!hybridInstance;
export const hybridHandler = Android.instance ? Android : iOS;
