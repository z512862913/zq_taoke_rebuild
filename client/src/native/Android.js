
import Promise from "promise";

const instance = window.AndroidClient;

export default {
    instance,
    _instance: instance || {},
    getAccountNumber() {
        if (this._instance.getSid instanceof Function) {
            return Promise.resolve(this._instance.getSid());
        }
        return Promise.resolve();
    },
    getAccountPassword() {
        if (this._instance.getPassword instanceof Function) {
            return Promise.resolve(this._instance.getPassword());
        }
        return Promise.resolve();
    },
    getAccount() {
        return Promise.all([
            this.getAccountNumber(),
            this.getAccountPassword(),
        ]);
    },
    saveAccount(id, password) {
        if (this._instance.saveUser instanceof Function) {
            this._instance.saveUser(id, password);
        }
    },
    back() {
        if (this._instance.back instanceof Function) {
            this._instance.back();
        }
    },
    setBackCode(code) {
        if (this._instance.setBackCode instanceof Function) {
            this._instance.setBackCode(code);
        }
    },
    shareCourse(name, url, content) {
        if (this._instance.share instanceof Function) {
            this._instance.share(name, url, content);
        }
    },
    finishLoading() {
        if (this._instance.onLoadFinished instanceof Function) {
            this._instance.onLoadFinished();
        }
    },
    setStatusBarColor(clr) {
        if (this._instance.setStatusBarColor instanceof Function) {
            this._instance.setStatusBarColor(clr);
        }
    },
};
