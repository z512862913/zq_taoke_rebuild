
import Promise from "promise";
import { getSafeValue } from "util";

const instance = getSafeValue(window, "webkit.messageHandlers.iOSModel");
const _instance = instance || {};
function defaultMessageHandler({ callback }) {
    if (callback instanceof Function) {
        callback();
    }
}

if (!(_instance.postMessage instanceof Function)) {
    _instance.postMessage = defaultMessageHandler;
}

export default {
    instance,
    getAccountNumber() {
        return new Promise(resolve => {
            window._getAccountNumber = number => resolve(number);
            _instance.postMessage({
                message: "getSid",
                callback: "window._getAccountNumber",
            });
        });
    },
    getAccountPassword() {
        return new Promise(resolve => {
            window._getAccountPassword = password => resolve(password);
            _instance.postMessage({
                message: "getPassword",
                callback: "window._getAccountPassword",
            });
        });
    },
    getAccount() {
        return Promise.all([
            this.getAccountNumber(),
            this.getAccountPassword(),
        ]);
    },
    saveAccount(number, password) {
        _instance.postMessage({
            message: "saveUser",
            arguments: [ number, password ],
        });
    },
    back() {
        _instance.postMessage({ message: "back" });
    },
    setBackCode(code) {
        _instance.postMessage({
            message: "setBackCode",
            arguments: [ code ],
        });
    },
    shareCourse(name, url, content) {
        _instance.postMessage({
            message: "share",
            arguments: [ name, url, content ],
        });
    },
    finishLoading() {
        _instance.postMessage({ message: "onLoadFinished" });
    },
    setStatusBarColor(clr) {
        _instance.postMessage({
            message: "setStatusBarColor",
            arguments: [ clr ],
        });
    },
};
