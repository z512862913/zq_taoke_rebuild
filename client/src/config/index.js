export const router = {
    linkActiveClass: "selected",
    suppressTransitionError: true,
};

export const pagination = {
    pc: {
        publicElective: 20,
        publicElectiveComment: 10,
        publicElectiveStudent: 16,
        professional: 15,
        source: 4,
    },
    webapp: {
        publicElective: 40,
    },
};

export const message = {
    source: {
        emptyNotice: "目前未收集到该类别的复习资料，如果愿意提供，请在用户反馈中填写~",
    },
    error: {
        api: "啊哦~服务器突然出了点故障╥﹏╥...",
        unknown: "啊哦~出现了未知错误╥﹏╥...",
    },
};

export const code = {
    publicElective: {
        NOT_IN_SELECTING_SEASON: 1,
        COLLECTED_AFTER_SAVE_SELECTING_FAILED: "1",
    },
};
