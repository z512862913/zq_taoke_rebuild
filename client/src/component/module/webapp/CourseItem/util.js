export function isValid(...values) {
    return values.every(value => {
        const type = typeof value;
        return (
            (
                (type === "string") && !!value.length
            ) ||
            (type === "number")
        );
    });
}

export function formatTime(course) {
    const {
        weekFrom,
        weekTo,
        weekday,
        classBegin,
        classOver,
    } = course;
    const weekdayMap = [
        "", "一", "二", "三", "四", "五", "六", "日",
    ];

    const items = [];
    if (isValid(weekFrom, weekTo)) {
        items.push(`${weekFrom}-${weekTo}周`);
    }
    if (isValid(weekday)) {
        items.push(`周${weekdayMap[weekday]}`);
    }
    if (isValid(classBegin, classOver)) {
        items.push(`${classBegin}-${classOver}节`);
    }

    return items.join("，");
}

export function formatTimeLocation(course) {
    const time = formatTime(course);
    const { location } = course;
    const items = [];
    if (isValid(time)) {
        items.push(time);
    }
    if (isValid(location)) {
        items.push(location);
    }
    return (!!items.length ? items.join("，") : "未知");
}
