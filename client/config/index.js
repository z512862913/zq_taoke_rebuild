// see http://vuejs-templates.github.io/webpack for documentation.
var path = require("path")

module.exports = {
    build: {
        env: require("./prod.env"),
        pc: path.resolve(__dirname, "../../templates/pc.html"),
        webapp: path.resolve(__dirname, "../../templates/webapp.html"),
        error: path.resolve(__dirname, "../../templates/error.html"),
        assetsRoot: path.resolve(__dirname, "../../asset/"),
        assetsSubDirectory: "",
        assetsPublicPath: "/static/",
        productionSourceMap: false,
    },
    dev: {
        env: require("./dev.env"),
        port: 80,
        staticRoot: path.resolve(__dirname, "../../static"),
        staticPublicPath: "/static",
        mediaRoot: path.resolve(__dirname, "../../media"),
        mediaPublicPath: "/media",
        assetsRoot: "/",
        assetsSubDirectory: "./static/",
        assetsPublicPath: "/",
        proxyList: [
            {
                context: [ "/api", "/static", "/media" ],
                options: {
                    target: "http://localhost:2333",
                },
            },
        ],
    },
};
