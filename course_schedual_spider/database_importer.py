# -*- coding: utf-8 -*-

import json
from course.models import Academy, Teacher, PublicElectiveCourseSet, PublicElectiveCourse, PublicElectiveCourseTime


class DataBaseImporter(object):

    @staticmethod
    def import_public_elective_course():
        data = json.load(open('public_elective_course_data.json'))
        for course_data in data:
            try:
                # 课程已经存在
                PublicElectiveCourse.objects.get(number=course_data['number'])
                continue
            except PublicElectiveCourse.DoesNotExist:
                try:
                    academy = Academy.objects.get(name=course_data['academy'])
                except Academy.DoesNotExist:
                    academy = Academy(name=course_data['academy'])
                    academy.save()
                try:
                    teacher = Teacher.objects.get(name=course_data['teacher'])
                except Teacher.DoesNotExist:
                    teacher = Teacher(name=course_data['teacher'], title=course_data['title'])
                    teacher.save()
                course_set_result = PublicElectiveCourseSet.objects.filter(name=course_data['name'],
                                                                           teacher=teacher, academy=academy,
                                                                           field=course_data['field'],
                                                                           credit=course_data['credit'])
                if len(course_set_result) == 0:
                    course_set = PublicElectiveCourseSet(name=course_data['name'], teacher=teacher, academy=academy,
                                                         field=course_data['field'], credit=course_data['credit'],
                                                         is_open=True)
                    course_set.save()
                else:
                    course_set = course_set_result[0]
                    course_set.is_open = True
                    course_set.save()
                try:
                    course = PublicElectiveCourse.objects.get(number=course_data['number'])
                except PublicElectiveCourse.DoesNotExist:
                    course = PublicElectiveCourse(name=course_data['name'], number=course_data['number'],
                                                  teacher=teacher, academy=academy, course_set=course_set,
                                                  field=course_data['field'], credit=course_data['credit'],
                                                  book=course_data['book'], is_open=True,
                                                  left=course_data['left'], total=course_data['total'])
                    course.save()
                for course_time in course_data['time']:
                    course_time = PublicElectiveCourseTime(class_begin=course_time['class_begin'],
                                                           class_over=course_time['class_over'],
                                                           week_from=course_time['week_from'],
                                                           week_to=course_time['week_to'],
                                                           weekday=course_time['weekday'],
                                                           repeat=course_time['repeat'],
                                                           location=course_time['location'],
                                                           course=course)
                    course_time.save()
        return True

    @staticmethod
    def refresh_public_elective_course_left_number():
        data = json.load(open('public_elective_course_data.json'))
        for course_data in data:
            course = PublicElectiveCourse.objects.get(number=course_data['number'])
            course.left = course_data['left']
            course.total = course_data['total']
            course.save()
        return True

    @staticmethod
    def adjust_is_open():
        data = json.load(open('public_elective_course_data.json'))
        number_list = []
        for course_data in data:
            number_list.append(course_data['number'])
        PublicElectiveCourse.objects.all().update(is_open=False)
        PublicElectiveCourseSet.objects.all().update(is_open=False)
        PublicElectiveCourse.objects.filter(number__in=number_list).update(is_open=True)
        PublicElectiveCourseSet.objects.filter(courses__number__in=number_list).update(is_open=True)


