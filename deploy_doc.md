# zq_taoke_rebuild部署文档

## 向负责人申请将本机ip加入mysql数据库ip白名单

## 前置软件包安装

+ 执行

    ```
    apt-get update  # 首先更新源
    apt-get install -y htop  # 安装系统负载监视工具（可选）
    apt-get install -y ipython  # 安装ipython（可选）
    apt-get install -y git  # 安装 Git
    apt-get install -y python-pip python-dev python-virtualenv  # 安装 Python 开发环境
    apt-get install -y nginx uwsgi uwsgi-plugin-python  # 安装 Nginx 和 uWSGI
    apt-get install -y supervisor  # 安装 Supervisor
    apt-get install -y redis-server  # 安装 Redis
    apt-get install -y libmysqlclient-dev  # 安装 MySQL 开发环境
    apt-get install -y libjpeg8-dev zlib1g-dev libfreetype6-dev  # 安装 Pillow 前置 tool 包
    apt-get install -y libxml2-dev libxslt-dev  # 安装lxml 前置依赖包
    apt-get install -y tesseract-ocr  # 安装 Tesseract-ocr
    ```
    
## 建立目录并下载源码

1. 执行

    ```
    mkdir -p /www/zq_taoke_rebuild
    cd /www/zq_taoke_rebuild
    virtualenv environment --system-site-packages  # 注意 --system-site-packages 参数，不然无法从系统环境继承
    mkdir static
    mkdir media
    git clone https://z512862913@bitbucket.org/z512862913/zq_taoke_rebuild.git web
    ```

1. 向负责人获取git仓库中缺失的 settings.py 并放入相应的位置

## python 包安装

+ 执行
    
    ```
    cd /www/zq_taoke_rebuild/web
    source ../environment/bin/activate  # 进入vitrualenv
    pip install -r requirements.txt
    ```

## 前端 nodejs 安装 & 部署

+ 参考文档 https://github.com/creationix/nvm

+ 执行

    ```
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash 
    export NVM_DIR="$HOME/.nvm"
    nvm install 6
    cd /www/zq_taoke_rebuild/web/client/
    npm install
    cd /www/zq_taoke_rebuild/web/
    ./build_frontend.sh
    ```

## 配置Nginx

1. 在 /etc/nginx/sites-avaliable 中新建文件 zq_taoke_rebuild 并向其中写入如下内容：

    ```
    server {
        listen 80;
        server_name taoke.ziqiang.studio wap.taoke.ziqiang.studio taoke.ziqiang.net.cn wap.taoke.ziqiang.net.cn;
        access_log /var/log/nginx/zq_taoke_rebuild.access.log;
        error_log /var/log/nginx/zq_taoke_rebuild.error.log;
    
        location / {
            uwsgi_pass  unix:///tmp/zq_taoke_rebuild.sock;
            include     uwsgi_params;
        }
    
    
        location /static/ {
            alias /www/zq_taoke_rebuild/static/;
        }
    
        location /media/ {
            alias /www/zq_taoke_rebuild/web/media/;
        }
    }
    ```
1. 执行

    ```
    ln -s /etc/nginx/sites-available/zq_taoke_rebuild /etc/nginx/sites-enabled/  # 注意这里必须使用绝对路径
    service nginx restart
    ```
    
## 修正目录权限

+ 执行

    ```
    chown www-data:www-data /www/zq_taoke_rebuild -R
    ```

## 配置 uWSGI

+ 创建文件夹 /etc/uwsgi/conf/ 在其中创建文件 zq_taoke_rebuild.ini 写入如下内容

    ```
    [uwsgi]
    vhost = true
    plugins = python
    socket = /tmp/zq_taoke_rebuild.sock
    master = true
    enable-threads = true
    processes = 8  # 这个参数可以按需更改
    virtualenv = /www/zq_taoke_rebuild/environment/
    chdir = /www/zq_taoke_rebuild/web/
    wsgi-file = /www/zq_taoke_rebuild/web/zq_taoke_rebuild/wsgi.py
    ```

## 配置 Supervisor

1. 创建 /etc/supervisor/conf.d/zq_taoke_rebuild.conf 并写入如下内容（可自行去掉注释）

    ```
    [program:zq_taoke_rebuild]
    user=www-data
    command=uwsgi --ini /etc/uwsgi/conf/zq_taoke_rebuild.ini
    process_name=%(program_name)s ; process_name expr (default %(program_name)s)
    numprocs=1                    ; number of processes copies to start (def 1)
    autostart=true                ; start at supervisord start (default: true)
    autorestart=true        ; whether/when to restart (default: unexpected)
    stopsignal=QUIT               ; signal used to kill process (default TERM)
    redirect_stderr=true          ; redirect proc stderr to stdout (default false)
    stdout_logfile=/var/log/web/zq_taoke_rebuild.log        ; stdout log path
    ```
    
1. 执行

    ```
    mkdir -p /var/log/web
    supervisorctl update
    supervisorctl restart zq_taoke_rebuild
    ```


## 将 tesseract 训练文件放置到正确的位置

+ 参见 /www/zq_taoke_rebuild/web/course_schedual_spider/tesseract_data/readMe 文件的说明

## 生成各种缓存

+ 执行

    ```
    cd /www/zq_taoke_rebuild/web/
    source ../environment/bin/activate  # 进入vitrualenv
    python manage.py shell  # 打开django shell
    ```
    
    1. 在shell中执行
    
        ```
        from course.models import PublicElectiveCourseSet
        PublicElectiveCourseSet.get_top_ten(force=True)  # 刷新前十排行榜的缓存   
        ```
    
## 加入定时任务

+ 参考文档 https://github.com/kraiz/django-crontab

+ 执行

    ```
    cd /www/zq_taoke_rebuild/web/
    source ../environment/bin/activate 进入vitrualenv
    python manage.py crontab add
    ```
    
## 将用户自定义头像文件进行迁移

+ 文件存在于 /www/zq_taoke_rebuild/web/media/img/hand_img/
