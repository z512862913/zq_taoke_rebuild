# -*- coding: utf-8 -*-
from functools import wraps

from django.core.cache import cache

from APIs import *
from APIErrorBase import APIError
from APIErrors import *


def api_error_handler(method):
    """
    自动处理一部分api错误并重发请求
    自动处理并重发请求的有：TokenExpireError, InvalidToken
    清空数据库中的密码和token并返回错误的是：EduPwdError 另：这个错误会被raise出来，请捕获并处理
    其他一律捕捉错误，返回错误代码
    另：重发请求的次数为一次，若第二次失败则直接返回错误代码
    :param method:
    :return: result, success
    如果成功result是返回的data
    如果失败result返回的是错误代码
    """
    # 此wraps装饰器是快速传递函数的元数据的
    @wraps(method)
    def wrapper(student):
        try:
            result = method(student)
            return result, True
        except APIError as error:
            if isinstance(error, EduPwdError):
                student.password = ''
                student.token = ''
                student.rf_token = ''
                student.save()
                raise EduPwdError(student)
            elif isinstance(error, InvalidToken) and student.password == '':
                # 空密码去rebind token, API 会返回None, get_result的时候json化会炸, 所以直接拦下来
                raise EduPwdError(student)
            else:
                try:
                    if isinstance(error, TokenExpireError):
                        student.refresh_token()
                        result = method(student)
                        return result, True
                    elif isinstance(error, InvalidToken):
                        # rebind_token 需要多加一次EduPwdError保护
                        try:
                            student.rebind_token()
                        except APIError as error:
                            if isinstance(error, EduPwdError):
                                raise EduPwdError(student)
                            else:
                                return error.error_code, False
                        result = method(student)
                        return result, True
                    else:
                        return error.error_code, False
                except APIError, error:  # 重发请求捕获错误信息
                    return error.error_code, False

    return wrapper


class API(object):
    # 双下划线开头，不超过一个下划线结尾为私有函数
    __get_token_ = GetTokenAPI()
    __refresh_token_ = RefreshTokenAPI()
    __check_token_status_ = CheckTokenStatusAPI()
    __change_pwd_ = ChangePwdAPI()
    __get_course_ = GetCourseAPI()
    __get_gpa_ = GetGpaAPI()
    __get_gender_ = GetGenderAPI()
    __get_information_ = GetInformationAPI()
    __get_public_elective_course_detail_ = GetPublicElectiveCourseDetailAPI()
    __get_public_elective_course_selection_result_ = GetPublicElectiveCourseSelectionResultAPI()
    __select_public_selective_course_ = SelectPublicSelectiveCourseAPI()

    @staticmethod
    @api_error_handler
    def get_token(student):
        return API.__get_token_(student)

    @staticmethod
    @api_error_handler
    def refresh_token(student):
        return API.__refresh_token_(student)

    @staticmethod
    @api_error_handler
    def check_token_status(student):
        return API.__check_token_status_(student)

    @staticmethod
    @api_error_handler
    def change_pwd(student):
        return API.__change_pwd_(student)

    @staticmethod
    @api_error_handler
    def get_course(student):
        # 失效相关的缓存
        # cache.set('%s_get_selective_course_credit_count' % student.number, None)
        # cache.set('%s_get_all_specialized_course' % student.number, None)
        # cache.set('%s_get_all_unscored_public_courses' % student.number, None)
        return API.__get_course_(student)

    @staticmethod
    @api_error_handler
    def get_gpa(student):
        return API.__get_gpa_(student)

    @staticmethod
    @api_error_handler
    def get_gender(student):
        return API.__get_gender_(student)

    @staticmethod
    @api_error_handler
    def get_information(student):
        return API.__get_information_(student)

    @staticmethod
    @api_error_handler
    def get_public_elective_course_detail(student):
        return API.__get_public_elective_course_detail_(student)

    @staticmethod
    @api_error_handler
    def get_public_elective_course_selection_result(student):
        # 失效相关的缓存
        # cache.set('%s_get_selective_course_credit_count' % student.number, None)
        # cache.set('%s_get_all_specialized_course' % student.number, None)
        # cache.set('%s_get_all_unscored_public_courses' % student.number, None)
        # cache.set('%s_get_public_elective_selection_failed_courses_from_database' % student.number, None)
        return API.__get_public_elective_course_selection_result_(student)

    @staticmethod
    @api_error_handler
    def select_public_selective_course(student):
        return API.__select_public_selective_course_(student)
