# -*- coding: utf-8 -*-
from APIErrorBase import APIError
__author__ = 'fuxiuyin'


class InvalidToken(APIError):

    error_code = -1

    def __init__(self, student):
        super(InvalidToken, self).__init__("InvalidToken: student %s with %s return invalid token" %
                                           (student.number, student.token))


class Forbidden(APIError):

    error_code = -2

    def __init__(self, student):
        super(Forbidden, self).__init__("Forbidden: student %s with %s return forbidden" %
                                        (student.number, student.token))


class NeedPwd(APIError):

    error_code = -3

    def __init__(self, student):
        super(NeedPwd, self).__init__("NeedPwd: student %s return 'must provide at least one password'" %
                                      student.number)


class UserDoesNotExist(APIError):

    error_code = -4

    def __init__(self, student):
        super(UserDoesNotExist, self).__init__("UserDoesNotExist: student %s return 'not exist'" %
                                               student.number)


class EduSysError(APIError):

    error_code = -5

    def __init__(self):
        super(EduSysError, self).__init__("EduSysError: education system is not available now")


class EduPwdError(APIError):

    error_code = -6

    def __init__(self, student):
        super(EduPwdError, self).__init__("EduPwdError: student %s with pwd %s return pwd error" %
                                          (student.number, student.password))


class CaptchaError(APIError):

    error_code = -7

    def __init__(self):
        super(CaptchaError, self).__init__("CaptchaError: captcha error")


class APINotAvailable(APIError):

    error_code = -8

    def __init__(self, response):
        super(APINotAvailable, self).__init__("APINotAvailable: can not get resource from api, it return %s" %
                                              response.status_code)


class InvalidStudentId(APIError):

    error_code = -9

    def __init__(self, student):
        super(InvalidStudentId, self).__init__("InvalidStudentId: student %s is invalid" %
                                               student.number)


class PermissionError(APIError):

    error_code = -10

    def __init__(self, student, permission):
        super(PermissionError, self).__init__("PermissionError: student %s with token %s don't have permission %s" %
                                              (student.number, student.token, permission))


class UnknownError(APIError):

    error_code = -11

    def __init__(self):
        super(UnknownError, self).__init__("UnknownError: unknown error")


class TokenExpireError(APIError):

    error_code = -12

    def __init__(self, student):
        super(TokenExpireError, self).__init__("TokenExpireError: student %s with token %s has expired" %
                                               (student.number, student.token))
