# -*- coding: utf-8 -*-
import time
import base64
import hmac
import hashlib
import urllib

from django.db import models
from aliyun import AliyunAPI
from course.models import Academy
from course.models import static_data_cache
import zq_taoke_rebuild.settings as settings

# Create your models here.


class SourceFirstLevelTag(models.Model):
    name = models.CharField(verbose_name=u'一级目录名称', max_length=20)
    icon_path = models.CharField(verbose_name=u'图标存放路径', max_length=60,
                                 blank=True, null=True)
    active_icon_path = models.CharField(verbose_name=u'图标存放路径', max_length=60,
                                        blank=True, null=True)

    @staticmethod
    @static_data_cache
    def get_all_tag():
        data = []
        for first_level_tag in SourceFirstLevelTag.objects.all().prefetch_related('sourcesecondleveltag_set'):
            sub_data = first_level_tag.get_first_level_tag_detail()
            sub_data['subcategories'] = []
            for second_level_tag in first_level_tag.sourcesecondleveltag_set.all():
                sub_data['subcategories'].append(second_level_tag.get_second_level_tag_detail())
            data.append(sub_data)
        return data

    def get_first_level_tag_detail(self):
        data = {
            'id': self.id,
            'name': self.name,
            'icon_path': self.icon_path,
            'active_icon_path': self.active_icon_path,
        }
        return data

    class Meta:
        verbose_name = u"一级目录"
        verbose_name_plural = u"一级目录"

    def __unicode__(self):
        return u'%s一级目录' % self.name


class SourceSecondLevelTag(models.Model):
    name = models.CharField(verbose_name=u'二级目录名称', max_length=20)
    parent = models.ForeignKey(verbose_name=u'上级目录', to=SourceFirstLevelTag)
    empty_notice = models.TextField(verbose_name=u'空目录提示文案', null=True, blank=True)

    def get_second_level_tag_detail(self):
        return {
            'id': self.id,
            'name': self.name,
            'empty_notice': self.empty_notice,
        }

    class Meta:
        verbose_name = u"二级目录"
        verbose_name_plural = u"二级目录"

    def __unicode__(self):
        return u'%s二级目录' % self.name

    def get_all_file(self, student_academy_id=None, per_page=-1, page=1):
        query_filter = models.Q(academy__isnull=True)
        query_filter |= models.Q(academy_id=student_academy_id)
        query_set = self.sourcefile_set.filter(query_filter)
        total_count = query_set.count()
        detail_list = []
        if per_page != -1:
            total_page = total_count / per_page
            if total_count % per_page:
                total_page += 1
            if not total_page:  # 排除总数为0的切片错误
                total_page = 1
            if page > total_page:
                page = total_page
            elif page < 1:
                page = 1
            for source_file in query_set[per_page*(page-1):per_page*page]:
                file_detail = source_file.get_file_detail()
                if file_detail:
                    detail_list.append(file_detail)
            result = {
                'page': page,
                'total_page': total_page,
                'list': detail_list,
            }
            return result
        else:
            for source_file in query_set:
                file_detail = source_file.get_file_detail()
                if file_detail:
                    detail_list.append(file_detail)
            result = {
                'page': 1,
                'total_page': 1,
                'list': detail_list,
            }
            return result


class SourceFile(models.Model):
    name = models.CharField(verbose_name=u'文件名称', max_length=50)
    tag = models.ForeignKey(verbose_name=u'所属二级目录', to=SourceSecondLevelTag)
    download_count = models.IntegerField(verbose_name=u'下载次数', default=0)
    # 学院专属的文件该字段不为空
    academy = models.ForeignKey(verbose_name=u'文件所属的学院', to=Academy, null=True, blank=True)

    class Meta:
        verbose_name = u"资源文件"
        verbose_name_plural = u"资源文件"
        ordering = ['-id']

    def __unicode__(self):
        return u'%s资源文件' % self.name

    @staticmethod
    def front_end_signature(file_id, expire_time):
        # 这里是处理和前端之间的连接交互的访问合法性保护和链接过期保护
        id_multiply_expire_time = int(file_id) * int(expire_time)
        key_hash_instance = hmac.new('my_taokela_key!@#$%^&*()', str(id_multiply_expire_time), hashlib.sha1)
        return urllib.quote(base64.encodestring(key_hash_instance.digest()).strip())

    def get_file_detail(self):
        extension = str(self.name.split('.')[-1])
        try:
            icon_path = settings.ICON_PATH_MAP[extension]
        except KeyError:
            icon_path = settings.ICON_PATH_MAP['other']
        expire_time = int(time.time())+settings.ALIYUN_OSS['CUSTOMIZED_URL_EXPIRE_TIME']
        standard_key = SourceFile.front_end_signature(self.id, expire_time)
        data = {
            'id': self.id,
            'sig': standard_key,  # 此key是用来防止简单的id遍历下载, 并且使链接会过期
            'name': self.name,
            'download_count': self.download_count,
            'icon_path': icon_path,
            'exp_time': int(time.time()) + settings.ALIYUN_OSS['CUSTOMIZED_URL_EXPIRE_TIME'],
        }
        return data

    def get_download_url(self, key, expire_time):
        """
        获取url，download_count加一
        :return: string: url
        """
        standard_key = SourceFile.front_end_signature(self.id, expire_time)
        if standard_key == key:
            url = self.get_oss_url()
            self.download_count += 1
            self.save()
            return url
        else:
            raise SourceFile.DoesNotExist

    def get_oss_url(self):
        file_path = u'/%s/%s/%s' % (self.tag.parent.name, self.tag.name, self.name)
        return AliyunAPI.get_oss_url(file_path=file_path)
