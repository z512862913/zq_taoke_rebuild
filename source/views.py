# -*- coding: utf-8 -*-
import time
import json

from django.http import JsonResponse
from django.views.decorators.cache import cache_page

from models import SourceFirstLevelTag, SourceSecondLevelTag, SourceFile
from suggestion.models import ErrorCodeToMessage
from student.views import customized_login_required

import zq_taoke_rebuild.settings as settings

# Create your views here.


@customized_login_required
def get_all_tag(request):
    """
    返回公共资源的一二级目录
    :param request:
    :return: 标准json返回
    示例返回：
    {
        "status": {
            "code": 0,
            "message": "获取公共资源类别列表成功"
        },
        'categories': {
            '一级目录名1': {
                'name': '一级目录名1',
                'first_level_tag_id': 1,
                'icon_path': 'static/icon.png',
                'children': [
                    {
                        'name': '二级目录名1',
                        'second_level_tag_id': 1,
                    },
                    {
                        'name': '二级目录名2',
                        'second_level_tag_id': 2,
                    },
                ]
            },
            '一级目录名2': {
                'name': '一级目录名2',
                'first_level_tag_id': 2,
                'icon_path': 'static/icon.png',
                'children': [
                    {
                        'name': '二级目录名3',
                        'second_level_tag_id': 3,
                    },
                    {
                        'name': '二级目录名4',
                        'second_level_tag_id': 4,
                    },
                ]
            },
        }
    }
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    data = SourceFirstLevelTag.get_all_tag()
    status['message'] = '获取公共资源列表成功'
    result['categories'] = data
    return JsonResponse(result, status=200)


# 缓存60s是因为url失效时间为120s
@cache_page(60)
@customized_login_required
def get_source_list(request, category_id, subcategory_id):
    """
    获取某个目录某一页下的所有资源
    :param request:
    :param category_id: 参数传入某个一级目录的id
    :param subcategory_id: 参数传入某个二级目录的id
    :return:
    示例返回：
    {
        "status": {
            "code": 0,
            "message": "获取公共资源列表成功",
        },
        'sources':
            {
                'list':
                    [
                        {
                            'id': 1,
                            'name': '资源1',
                            'download_count': 0,
                            'icon_path': 'file_type_icon/fileicon-word.png',
                            'key': 'WVpGWbMpagaPNtg4YGKZvkjZ2kU%3D',
                            'expire_time': 1468065744,
                        },
                        {
                            'id': 2,
                            'name': '资源2',
                            'download_count': 0,
                            'icon_path': 'file_type_icon/fileicon-pdf.png',
                            'key': 'X1%2B/Gv3NF1r46jMoTvo5K0fMZX4%3D',
                            'expire_time': 1468065744,
                        },
                        {
                            'id': 3,
                            'name': '资源3',
                            'download_count': 0,
                            'icon_path': 'file_type_icon/fileicon-ppt.png',
                            'key': 'lui%2BEQuQw%2BQhsoWt1I6wcf8xvAM%3D',
                            'expire_time': 1468065744,
                        },
                        {
                            'id': 4,
                            'name': '资源4',
                            'download_count': 0,
                            'icon_path': 'file_type_icon/fileicon-rar.png',
                            'key': 'WUiIL0aD4AYrtK4rc7qXcmFDFBE%3D',
                            'expire_time': 1468065744,
                        },
                    ],
                'num_pages': 4,
            }
    }
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'GET':
        per_page = int(request.GET.get('per_page', -1))
        page = int(request.GET.get('page', 1))
        try:
            first_tag = SourceFirstLevelTag.objects.get(id=category_id)
            second_level_tag = first_tag.sourcesecondleveltag_set.get(id=subcategory_id)
        except SourceFirstLevelTag.DoesNotExist, SourceSecondLevelTag.DoesNotExist:
            status['code'] = 666
            status['message'] = 'no such tag'
            return JsonResponse(result, status=400)
        data = second_level_tag.get_all_file(student_academy_id=student.academy_id, per_page=per_page, page=page)
        status['message'] = '获取公共资源列表成功'
        result['source'] = data['list']
        result['pagination'] = {
            'page': data['page'],
            'num_pages': data['total_page']
        }
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


def download_source(request, category_id, subcategory_id, source_id):
    """
    需要登录状态
    :param request: 需要GET参数
                sig: 直接传给前端的key
                exp_time: unix时间戳，链接过期时间
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.user.is_authenticated():
        if request.method == 'GET':
            key = request.GET.get('sig', None)
            expire_time = request.GET.get('exp_time', None)
            if source_id and key and expire_time:
                if int(time.time()) < int(expire_time):
                    try:
                        source_file = SourceFile.objects.get(id=source_id)
                        url = source_file.get_download_url(key=key, expire_time=expire_time)
                    except SourceFile.DoesNotExist:
                        status['code'] = 4
                        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(4)
                        return JsonResponse(result, status=403)
                    # 这里才是正确的返回
                    status['message'] = '获取公共资源成功'
                    result['url'] = url
                    return JsonResponse(result, status=200)
                else:
                    # 下载链接失效
                    status['code'] = 7
                    status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(7)
                    return JsonResponse(result, status=401)
            else:
                status['code'] = 666
                status['message'] = 'need argument source_id and key'
                return JsonResponse(result, status=400)
        else:
            # http方法不支持
            status['code'] = 10
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
            return JsonResponse(result, status=405)
    else:
        status['code'] = 9
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(9)
        return JsonResponse(result, status=401)








