# -*- coding: utf-8 -*-
from django.contrib import admin
from source.models import *


# Register your models here


@admin.register(SourceFirstLevelTag)
class SourceFirstLevelTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'icon_path',)


@admin.register(SourceSecondLevelTag)
class SourceSecondLevelTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent',)


@admin.register(SourceFile)
class SourceFileAdmin(admin.ModelAdmin):
    list_display = ('name', 'tag', 'download_count')
    search_fields = ('name',)
