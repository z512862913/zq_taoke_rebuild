# -*- coding: utf-8 -*-

from django.conf.urls import url

import views

urlpatterns = [
    url(r'^$', views.get_all_tag),
    url(r'^(\d{1,15})/(\d{1,15})/$', views.get_source_list),
    url(r'^(\d{1,15})/(\d{1,15})/(\d{1,15})/$', views.download_source)
]
