# -*- coding: utf-8 -*-

from course_schedual_spider import CourseScheduleAPI
from course.models import PublicElectiveCourseSet, PublicElectiveCourse, \
    PublicCompulsoryCourseSet, PublicCompulsoryCourse, SpecializedCourseSet, SpecializedCourse
import zq_taoke_rebuild.settings as settings


def change_semester():
    PublicElectiveCourseSet.objects.update(is_open=False)
    PublicElectiveCourse.objects.update(is_open=False)
    PublicCompulsoryCourseSet.objects.update(is_open=False)
    PublicCompulsoryCourse.objects.update(is_open=False)
    SpecializedCourseSet.objects.update(is_open=False)
    SpecializedCourse.objects.update(is_open=False)
    semester_id = str(settings.SEMESTER_DATE['YEAR']) + str(settings.SEMESTER_DATE['SEMESTER'])
    # 公选课还是用course_schedual_spider里的DataBaseImporter.adjust_is_open()
    # 不然会有奇怪的貌似是专业课然而标记成公选课却有不出现在公选课的列表中的奇怪的课冒出来
    # PublicElectiveCourseSet.objects.filter(courses__number__startswith=semester_id).update(is_open=True)
    # PublicElectiveCourse.objects.filter(number__startswith=semester_id).update(is_open=True)
    PublicCompulsoryCourseSet.objects.filter(courses__number__startswith=semester_id).update(is_open=True)
    PublicCompulsoryCourse.objects.filter(number__startswith=semester_id).update(is_open=True)
    SpecializedCourseSet.objects.filter(courses__number__startswith=semester_id).update(is_open=True)
    SpecializedCourse.objects.filter(number__startswith=semester_id).update(is_open=True)


def refresh_public_elective_course_total_left_count():
    CourseScheduleAPI.refresh_public_elective_course_total_left_count()