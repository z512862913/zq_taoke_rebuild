# -*- coding: utf-8 -*-

from django.conf.urls import url

import views

urlpatterns = [
    url(r'^public/elective/$', views.search),
    url(r'^public/elective/leader_board/$', views.leader_board),
    url(r'^public/elective/popular/$', views.popular),
    url(r'^public/elective/(\d{1,15})/$', views.public_elective_course_detail),
    url(r'^public/elective/(\d{1,15})/comment/$', views.comment),
    url(r'^public/elective/(\d{1,15})/student/$', views.get_students_list),
    url(r'^professional/$', views.get_com_course_list),
    url(r'professional/(\d{1,15})/$', views.get_specialized_course_detail),
    url(r'department/$', views.get_department_list),
    url(r'academy/$', views.get_academy_list)
]
