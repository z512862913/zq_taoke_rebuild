# -*- coding: utf-8 -*-
from django.contrib import admin
from course.models import *


# Register your models here.

@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'num',)
    search_fields = ('name',)


@admin.register(Academy)
class AcademyAdmin(admin.ModelAdmin):
    list_display = ('name', 'department')
    search_fields = ('name',)


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('name', 'title')
    search_fields = ('name',)


@admin.register(PublicElectiveCourseSet)
class PublicElectiveCourseSetAdmin(admin.ModelAdmin):
    list_display = ('name', 'teacher', 'academy', 'summary', 'credit', 'is_open', 'field',
                    'check_attendance_good', 'check_attendance_medium', 'check_attendance_bad',
                    'teaching_quality_good', 'teaching_quality_medium', 'teaching_quality_bad',
                    'giving_score_good', 'giving_score_medium', 'giving_score_bad', 'is_popular',
                    'is_check_attendance_top_ten', 'is_teaching_quality_top_ten', 'is_giving_score_top_ten',
                    'is_boy_top_ten', 'is_girl_top_ten',)
    list_filter = ('is_open', 'is_popular',
                   'is_check_attendance_top_ten', 'is_teaching_quality_top_ten', 'is_giving_score_top_ten',
                   'is_boy_top_ten', 'is_girl_top_ten',)
    search_fields = ('name', 'teacher__name', 'academy__name',)


@admin.register(SpecializedCourseSet)
class SpecializedCourseSetAdmin(admin.ModelAdmin):
    list_display = ('name', 'teacher', 'academy', 'summary', 'credit', 'is_open', 'type', 'major',
                    'recommend_count', 'star', 'update_date',)
    list_filter = ('is_open', 'star', 'type', 'major',)
    search_fields = ('name', 'teacher__name', 'academy__name', 'major',)


@admin.register(PublicCompulsoryCourseSet)
class PublicCompulsoryCourseSetAdmin(admin.ModelAdmin):
    list_display = ('name', 'teacher', 'academy', 'summary', 'credit', 'is_open',)


@admin.register(PublicElectiveCourse)
class PublicElectiveCourseAdmin(admin.ModelAdmin):
    list_display = ('number', 'name',  'book', 'teacher', 'academy', 'week_from', 'week_to', 'credit', 'is_open',
                    'course_set', 'field', 'left', 'total', 'is_popular',)
    list_filter = ('is_open', 'is_popular',)
    search_fields = ('name', 'number', 'teacher__name', 'academy__name',)


@admin.register(SpecializedCourse)
class SpecializedCourseAdmin(admin.ModelAdmin):
    list_display = ('number', 'name', 'book', 'teacher', 'academy', 'week_from', 'week_to', 'credit', 'is_open',
                    'type', 'course_set', 'major', 'grade',)
    list_filter = ('is_open', 'type')
    search_fields = ('name', 'number', 'teacher__name', 'academy__name',)


@admin.register(PublicCompulsoryCourse)
class PublicCompulsoryCourseAdmin(admin.ModelAdmin):
    list_display = ('number', 'name', 'book', 'teacher', 'academy', 'week_from', 'week_to', 'credit', 'is_open',
                    'course_set',)
    list_filter = ('is_open',)
    search_fields = ('name', 'number', 'teacher__name', 'academy__name',)


@admin.register(PublicElectiveCourseTime)
class PublicElectiveCourseTimeAdmin(admin.ModelAdmin):
    list_display = ('course', 'location', 'class_begin', 'class_over', 'weekday', 'repeat', 'week_from', 'week_to',)
    list_filter = ('course__is_open',)
    search_fields = ('course__number', 'course__name',)


@admin.register(SpecializedCourseTime)
class SpecializedCourseTimeAdmin(admin.ModelAdmin):
    list_display = ('course', 'location', 'class_begin', 'class_over', 'weekday', 'repeat', 'week_from', 'week_to',)
    list_filter = ('course__is_open',)
    search_fields = ('course__number', 'course__name',)


@admin.register(PublicCompulsoryCourseTime)
class PublicCompulsoryCourseTimeAdmin(admin.ModelAdmin):
    list_display = ('course', 'location', 'class_begin', 'class_over', 'weekday', 'repeat', 'week_from', 'week_to',)
    list_filter = ('course__is_open',)
    search_fields = ('course__number', 'course__name',)


@admin.register(PopularPublicElectiveCourse)
class PopularPublicElectiveCourseAdmin(admin.ModelAdmin):
    list_display = ('course',)
    actions = ['admin_delete']

    # 删除默认用的是QuerySet.delete(), 所以需要重写
    def admin_delete(self, request, queryset):
        for popular_course in queryset:
            popular_course.delete()

    admin_delete.short_description = u'删除热门课程(运维同学请使用这个方法)'





