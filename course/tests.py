# -*- coding: utf-8 -*-
from django.test import TestCase
from .models import *

# Create your tests here.


# 方法要用test开头，不是很懂怎么写
class DatabaseTest(TestCase):
    def test_create(self):
        academy1 = Academy(name='academy1')
        academy1.save()

        teacher = Teacher(name='teacher1', title='title')
        teacher.save()

        same_course_set1 = SameCourseSet(name='same_course_set1',
                                         teacher=teacher,
                                         academy=academy1)
        same_course_set1.save()

        course1 = Course(name=u'1', number=1, course_set=same_course_set1)
        course1.save()