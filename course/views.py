# -*- coding: utf-8 -*-
import json

from django.http import JsonResponse

from models import Department, Academy, PublicElectiveCourse, PublicElectiveCourseSet, PopularPublicElectiveCourse, \
    SpecializedCourse, SpecializedCourseSet
from suggestion.models import ErrorCodeToMessage
from student.views import customized_login_required


# Create your views here.


# @customized_login_required
def search(request):
    """
    搜索课程
    :param request: POST
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'POST':
        # 如果这个大列表也提供学生的相关信息，那么外表查询的消耗过大
        # 再说也不是很有必要
        # if request.user.is_authenticated():
        #     student = request.user.student
        # else:
        #     student = None
        if not request.body:
            request_data = {}
        else:
            request_data = json.loads(request.body)
        search_key = request_data.get('search_key', None)
        weekday = request_data.get('weekday', [])
        department = request_data.get('department', [])
        field = request_data.get('field', [])
        credit = request_data.get('credit', [])
        class_time = request_data.get('class_time', '')
        order = request_data.get('order', [])
        has_remain = request_data.get('only_remain', False)
        page = request_data.get('page', 1)
        per_page = request_data.get('per_page', -1)
        data = PublicElectiveCourse.course_filter(key_word=search_key, item_per_page=per_page, page=page,
                                                  weekday=weekday, location=department, field=field, credit=credit,
                                                  class_time=class_time, has_left=has_remain, order=order)
        result['public_elective'] = data['list']
        result['pagination'] = {
            'num_pages': data['total_page'],
            'page': data['current_page'],
        }
        status['message'] = '获取公选课列表成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


def leader_board(request):
    """
    获取排行榜
    :param request:
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    top_ten_data, success = PublicElectiveCourseSet.get_top_ten()
    if success:
        # 只有在登录状态和评论完所有公选课之后才能看到男女生的排行榜
        if request.user.is_authenticated() and not request.user.student.get_all_uncomment_course():
            pass
        else:
            top_ten_data['boy'] = None
            top_ten_data['girl'] = None
        status['message'] = '获取热门公选课列表成功'
        result['leader_board'] = top_ten_data
        return JsonResponse(result, status=200)
    else:
        error_code = top_ten_data
        status['code'] = error_code
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(11)
        return JsonResponse(result, status=400)


def popular(request):
    """
    返回热门课程
    :param request:
    :return:
    """
    status = {'code': 0, 'message': '获取热门公选课列表成功'}
    result = {
        'status': status,
        'popular': PopularPublicElectiveCourse.get_all_popular_course_with_other_course_option()
    }
    return JsonResponse(result, status=200)


def public_elective_course_detail(request, number):
    """
    取出一个课程的详情
    :param request: GET
    :param number: 课头号
    :return: 标准json返回
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    # 这里不直接get而是用filter是为了能使用select_related()和prefetch_related()两个方法来减少外键查询，从而减少sql的连接数
    # 至于单个model对象是不是也能prefetch，嗯，反正我翻文档没有找到的样子
    query_set = PublicElectiveCourse.objects.filter(number=number) \
        .select_related('teacher', 'academy', 'course_set').prefetch_related('course_time')
    if query_set:
        course = query_set[0]
    else:
        status['code'] = 4
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(4)
        return JsonResponse(result, status=400)
    data = course.get_course_detail()
    data['other_courses'] = course.get_other_course_in_the_same_course_set()
    if request.user.is_authenticated():
        student = request.user.student
        relations = student.get_relations_with_course(course=course)
    else:
        relations = {
            'has_collected': False,
            'has_circulated': False,
            'can_comment': False,
        }
    result.update(data)
    result.update(relations)
    status['message'] = '获取公选课信息成功'
    return JsonResponse(result, status=200)


def comment(request, number):
    """
    取出课程评论
    :param request:GET
    :param number:
    :return:
    """
    status = {'code': 0, 'message': '获取公选课评论列表成功'}
    result = {'status': status}
    if request.method == 'GET':
        page = int(request.GET.get('page', 1))
        per_page = int(request.GET.get('per_page', -1))
        try:
            course = PublicElectiveCourse.objects.get(number=number)
            comments, pagination = course.get_all_comment(page, per_page)
            result['comment'] = comments
            result['pagination'] = pagination
            return JsonResponse(result, status=200)
        except PublicElectiveCourse.DoesNotExist:
            status['code'] = 4
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(4)
            return JsonResponse(result, status=403)
    else:
        # http方法不支持
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


def get_students_list(request, number):
    """
    获取同上这门课的学生信息
    :param request: GET
    :param number: 课头号
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'GET':
        page = int(request.GET.get('page', 1))
        per_page = int(request.GET.get('per_page', -1))
        try:
            course = PublicElectiveCourse.objects.get(number=number)
            student_list, pagination = course.get_students_list(page=page, per_page=per_page)
            result['student'] = student_list
            result['pagination'] = pagination
            status['message'] = '获取公选课学生列表成功'
            return JsonResponse(result, status=200)
        except PublicElectiveCourse.DoesNotExist:
            status['code'] = 4
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(4)
            return JsonResponse(result, status=403)
    else:
        # http方法不支持
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


def get_com_course_list(request):
    """
    返回专业课课程列表
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'GET':
        page = int(request.GET.get('page', 1))
        per_page = int(request.GET.get('per_page', -1))
        academy = request.GET.get('academy', [])
        data, pagination = SpecializedCourse.get_course_list(page=page, per_page=per_page, academy=academy)
        result['professional'] = data
        result['pagination'] = pagination
        status['message'] = '获取专业课列表成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


# @customized_login_required
def get_specialized_course_detail(request, number):
    """
    获取专业课程详细信息
    :param request: GET
    :param number: 对应专业课课头号
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'GET':
        course = SpecializedCourse.objects.get(number=number)
        data = course.get_course_detail()
        data['rank'] = data['star']
        data.pop('star')
        status['message'] = '获取专业课信息成功'
        result.update(data)
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


def get_department_list(request):
    """
    获取学部列表
    这里没写错。。文档就是这样写的，和下面的一模一样 好像下面那个接口前端没在用
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'GET':
        department_list = Academy.get_department_and_academy_list()
        status['message'] = '获取学部列表成功'
        result['department'] = department_list
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


def get_academy_list(request):
    """
    获取学院列表
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'GET':
        status['message'] = '获取学部列表成功'
        result['department'] = Academy.get_department_and_academy_list()
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)




