# -*- coding: utf-8 -*-

from aliyun.message import MessageSender
from aliyun.oss import DownloadURLGetter


class AliyunAPI(object):
    __oss_url_getter = DownloadURLGetter()
    __message_sender = MessageSender()

    @staticmethod
    def get_oss_url(file_path):
        return AliyunAPI.__oss_url_getter.get_url(file_path)

    @staticmethod
    def send_message(student, course):
        # return AliyunAPI.__message_sender.send_success_message(student_number=student.number,
        #                                                        student_name=student.name,
        #                                                        course_number=course.number,
        #                                                        course_name=course.name,
        #                                                        phone_number=student.contact_number)
        return AliyunAPI.__message_sender.send_abstract_success_message(student_name=student.name,
                                                                        course_name=course.name,
                                                                        phone_number=student.contact_number)

    @staticmethod
    def send_public_elective_season_end_notice(student):
        return AliyunAPI.__message_sender.send_public_elective_season_end_notice(student_name=student.name,
                                                                                 phone_number=student.contact_number)

