# -*- coding: utf-8 -*-
import time
import base64
import hmac
import hashlib
import urllib

from aliyun.base import AliyunCommon


class DownloadURLGetter(AliyunCommon):
    REGION = 'shanghai'
    BUCKET = 'zq-taokela'
    # 'REGION': 'hangzhou',
    # 'BUCKET': 'taokela',
    URL_EXPIRE_TIME = 3600

    def get_url(self, file_path):
        expires = str(int(time.time()) + self.URL_EXPIRE_TIME)
        # hash_instance 接受的是字节，但是传入的是文本
        # 如果做隐式的转换会受到system default coding的影响
        # import sys
        # reload(sys)
        # sys.setdefaultencoding('utf8')
        # 拒绝使用上面的解决方法，因为那样容易引入新的bug
        # 所以最终的解决方法是使用显式的encode
        str_encode = (u'GET\n\n\n%s\n/%s%s' % (expires, self.BUCKET, file_path)).encode('utf-8')
        signature_hash_instance = hmac.new(self.ACCESS_KRY_SECRET, str_encode, hashlib.sha1)
        signature = urllib.quote_plus(base64.encodestring(signature_hash_instance.digest()).strip())
        url = 'http://%s.oss-cn-%s.aliyuncs.com/%s?OSSAccessKeyId=%s&Expires=%s&Signature=%s' % \
              (self.BUCKET, self.REGION, file_path, self.ACCESS_KEY_ID, expires, signature)
        return url
