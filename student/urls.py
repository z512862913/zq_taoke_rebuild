# -*- coding: utf-8 -*-

from django.conf.urls import url
from suggestion.views import add_feedback

import views

urlpatterns = [
    url(r'^$', views.get_stu_info),
    url(r'^avatar/$', views.avatar),
    url(r'^curriculum/$', views.curriculum),
    url(r'^public/elective/selected/$', views.get_all_public_selected_course),
    url(r'^public/elective/collected/$', views.get_liked_courses),
    url(r'^public/elective/collected/(\d{1,15})/$', views.operate_like_course),
    url(r'^public/elective/uncomment/$', views.get_uncomment_courses),
    url(r'^public/elective/uncomment/(\d{1,15})/$', views.comment_uncomment_course),
    url(r'^public/elective/selecting/$', views.get_selecting_and_failed_courses),
    url(r'^public/elective/selecting/(\d{1,15})/$', views.operate_selecting_public_courses),
    url(r'^professional/$', views.get_user_professional_course),
    url(r'^professional/recommended/$', views.get_all_recommend_courses_detail),
    url(r'^professional/recommended/(\d{1,15})/$', views.recommend_course),
    url(r'^feedback/$', add_feedback),
    url(r'^sms/$', views.operate_sms_service),
    url(r'^finish_rate/', views.finish_rate),
    url(r'^login_for_gm/', views.login_for_gm),
]
