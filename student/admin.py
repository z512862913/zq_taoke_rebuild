# -*- coding: utf-8 -*-

from django.contrib import admin
from student.models import *


# Register your models here.


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('number', 'name', 'gender', 'gpa', 'sign_in_time', 'sys_avatar', 'avatar', 'avatar_type_switch',
                    'academy', 'token', 'rf_token', 'selection_result_update_time',
                    'course_table_update_time', 'selection_list_submit_time',)
    filter_horizontal = ('liked_public_elective_courses', 'selection_fail_courses', 'circulate_selection_courses',)
    list_filter = ('gender',)
    search_fields = ('number', 'name',)


@admin.register(PublicElectiveStudentCourse)
class PublicElectiveStudentCourseAdmin(admin.ModelAdmin):
    list_display = ('student', 'course', 'course_set', 'learning_type', 'score', 'has_commented',)
    search_fields = ('student__name', 'student__number', 'course',)


@admin.register(SpecializedStudentCourse)
class SpecializedStudentCourseAdmin(admin.ModelAdmin):
    list_display = ('student', 'course', 'course_set', 'learning_type', 'score', 'has_recommended', 'recommend_time')
    search_fields = ('student__name', 'student__number', 'course',)


@admin.register(PublicCompulsoryStudentCourse)
class PublicCompulsoryStudentCourseAdmin(admin.ModelAdmin):
    list_display = ('student', 'course', 'course_set', 'learning_type', 'score',)
    search_fields = ('student__name', 'student__number', 'course',)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('course_set', 'course', 'student', 'student_course', 'content', 'is_anonymous', 'publish_time',
                    'check_attendance_valuation', 'teaching_quality_valuation', 'giving_score_valuation',
                    'is_anonymous',)
    search_fields = ('student__name', 'student__number',)
