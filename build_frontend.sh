#!/bin/bash
# Plz use './build_frontend.sh' instead of 'sh build_frontend.sh'
cd /www/zq_taoke_rebuild/web
git pull
cd ./client
npm run build
cd ..
source ../environment/bin/activate
python manage.py collectstatic --clear --noinput
