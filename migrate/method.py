# -*- coding: utf-8 -*-
from course.models import Department, Academy, Teacher,\
    PublicElectiveCourse, PublicElectiveCourseSet, PublicElectiveCourseTime,\
    SpecializedCourse, SpecializedCourseSet, SpecializedCourseTime, \
    PublicCompulsoryCourse, PublicCompulsoryCourseSet, PublicCompulsoryCourseTime


# def new_course(data):
#     """
#     此方法暂时是在用户获取课表的时候添加新课程
#     :param data: formed data from the API
#     :return: Course object created
#     """
#     try:
#         course = Course.objects.get(number=data['identifier'])
#         return course
#     except Course.DoesNotExist:
#         # 取出或者创建对应的教师对象
#         try:
#             teacher = Teacher.objects.get(name=data['instructor'])
#         except Teacher.DoesNotExist:
#             teacher = Teacher(name=data['instructor'])
#             teacher.save()
#         # 取出或者创建对应的学院对象
#         try:
#             academy = Academy.objects.get(name=data['college,,,,,'])
#         except Academy.DoesNotExist:
#             academy = Academy(name=data['college'])
#             academy.save()
#         # 取出或者创建对应的课程集合对象
#         try:
#             course_set = SameCourseSet.objects.get(name=data['name'], teacher=teacher, academy=academy,
#                                                    credit=data['credit'], course_type=data['course_type'])
#         except SameCourseSet.DoesNotExist:
#             course_set = SameCourseSet(name=data['name'], teacher=teacher, academy=academy,
#                                        credit=data['credit'], major=data['major'], course_type=data['course_type'])
#             course_set.save()
#         # 用判断是否有给分的方式来判断是否开课
#         if data['grade'] > 0:
#             is_open = False
#         else:
#             is_open = True
#         # 根据信息创建新课程
#         course = Course(number=data['identifier'], name=data['name'], course_set=course_set, book=data.get('book'),
#                         teacher=teacher, academy=academy, credit=data['credit'], major=data['major'],
#                         course_type=data['course_type'], is_open=is_open)
#         course.save()
#         # 创建上课时间信息
#         week_from = 100
#         week_to = 0
#         for course_time in data['lessons_time']:
#             new_course_time = CourseTime(course=course, location=course_time['location'],
#                                          class_begin=course_time['class_begin'], class_over=course_time['class_over'],
#                                          weekday=course_time['weekday'], repeat=course_time['repeats'],
#                                          week_from=course_time['week_from'], week_to=course_time['week_to'])
#             new_course_time.save()
#             if week_from > course_time['week_from']:
#                 week_from = course_time['week_from']
#             if week_to < course_time['week_to']:
#                 week_to = course_time['week_to']
#         # 如果有上课时间信息就可以推断出课程的开始结束周
#         if week_from != 100 and week_to:
#             course.week_from = week_from
#             course.week_to = week_to
#             course.save()
#         return course

